import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Pickers 1.0
import io.thp.pyotherside 1.4
import "../components"

Page {
    id: page
    allowedOrientations: Orientation.All
    property int margin: Theme.paddingMedium


    SilicaFlickable {
        id: mainflick
        anchors.fill: parent
        // contentHeight: page.height

        property bool backupRunning: false
        property bool opmlWriteRunning: false

        Connections {
            target: feedparserhandler
            onBackupDone: {
                console.log("Notify backup done")
                appNotification.previewSummary = qsTr("Backup done")
                appNotification.previewBody = qsTr("Backup done to ") + tarpath
                appNotification.body = qsTr("Backup done to ") + tarpath
                appNotification.publish()
                mainflick.backupRunning = false
            }
            onOpmlSaveDone: {
                console.log("Notify opml save done")
                appNotification.previewSummary = qsTr("OPML file saved")
                appNotification.previewBody = qsTr("OPML file saved to ") + opmlpath
                appNotification.body = qsTr("OPML file saved to ") + opmlpath
                appNotification.publish()
                mainflick.opmlWriteRunning = false
            }
        }

        AppMenu { thispage: "Backup" }
        PrefAboutMenu {}

        Row {
            id: discovertitle
            width: page.width
            Column {
                id: column
                width: page.width
                spacing: Theme.paddingLarge

                PageHeader {
                    title: qsTr("Export")
                }
            }
        }
        Column {
            id: backupb
            height: Theme.itemSizeLarge
            anchors.top: discovertitle.bottom
            visible: experimentalConf.value
            Button {
                id: importbutton
                text: qsTr("Backup")
                enabled: !mainflick.backupRunning
                onClicked: {
                    Remorse.popupAction(page, qsTr("Start Backup"), function() {
                        mainflick.backupRunning = true
                        feedparserhandler.doBackup()
                    })
                }
                BusyIndicator {
                    size: BusyIndicatorSize.Medium
                    anchors.centerIn: parent
                    running: mainflick.backupRunning
                }
            }
        }
        Column {
            id: opmlwrite
            height: Theme.itemSizeLarge
            anchors.top: backupb.bottom
            Button {
                id: opmlbutton
                text: qsTr("Save to OPML")
                enabled: !mainflick.opmlWriteRunning
                onClicked: {
                    Remorse.popupAction(page, qsTr("Start OPML export"), function() {
                        mainflick.opmlWriteRunning = true
                        feedparserhandler.doWriteOpml()
                    })
                }
                BusyIndicator {
                    size: BusyIndicatorSize.Medium
                    anchors.centerIn: parent
                    running: mainflick.opmlWriteRunning
                }
            }
        }
        PlayDockedPanel { }
    }
}
