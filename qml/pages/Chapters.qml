import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    Component.onCompleted: {
        chapterModel.clear()
        for (var i = 0; i < podqast.chapters.length; i++) {
            console.log(podqast.chapters[i].start_parsed)
            var thechapter = podqast.chapters[i]
            thechapter.id = i
            chapterModel.append(thechapter)
        }
    }
    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent
        anchors.leftMargin: 30

        // Tell SilicaFlickable the height of its content.
        contentHeight: page.height

        Column {
            id: queuetitle

            width: page.width

            spacing: Theme.paddingLarge
            PageHeader {
                title: qsTr("Chapters")
            }
        }
        SilicaListView {
            id: chapterslist
            anchors.top: queuetitle.bottom
            width: parent.width
            height: page.height - queuetitle.height
            currentIndex: podqast.aktchapter

            model: ListModel {
                id: chapterModel
            }
            delegate: ListItem {
                contentHeight: Theme.itemSizeLarge
                width: page.width
                clip: true
                onClicked: {
                    var millis = podqast.tomillisecs(start)
                    console.log(millis)
                    playerHandler.seek(millis)
                    mediaplayer.seek(millis)
                }

                Rectangle {
                    color: Theme.highlightBackgroundColor
                    visible: id === podqast.aktchapter
                    opacity: Theme.highlightBackgroundOpacity
                    anchors.fill: parent
                }

                Column {
                    height: parent.height
                    width: parent.width
                    Label {
                        anchors {
                            left: parent.left
                            right: parent.right
                            margins: Theme.paddingMedium
                        }
                        text: start.substr(0,8)
                        font.pixelSize: Theme.fontSizeTiny
                        wrapMode: Text.WordWrap
                    }
                    Label {
                        anchors {
                            left: parent.left
                            right: parent.right
                            margins: Theme.paddingMedium
                        }

                       text: title
                       font.pixelSize: Theme.fontSizeSmall
                       font.bold: true
                       wrapMode: Text.WordWrap
                    }
                }
            }
            ViewPlaceholder {
                text: qsTr("No chapters")
                hintText: qsTr("Rendering chapters")
            }

            // VerticalScrollDecorator {}
        }
    }
}
