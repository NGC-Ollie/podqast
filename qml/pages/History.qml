import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    Component.onCompleted: {
        // archivehandler.getArchiveEntries(podqast.hfilter)
    }

    Connections {
        target: archivehandler
        ignoreUnknownSignals : true
        onCreateArchiveList: {
            archivePostModel.clear()
            for (var i = 0; i < data.length; i++) {
                archivePostModel.append(data[i]);
            }
        }
    }

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent

        VerticalScrollDecorator { }
        PrefAboutMenu {}

        AppMenu { thispage: "Archive" }

        // Tell SilicaFlickable the height of its content.
        contentHeight: page.height

        Column {
            id: archivetitle

            width: page.width

            spacing: Theme.paddingLarge
            PageHeader {
                title: qsTr("History")
            }
        }

        PodSelectGrid {
            id: podselectgrid
            anchors.top: archivetitle.bottom
            width: parent.width
            height: Theme.iconSizeLarge
            z: 3
            homeicon: "image://theme/icon-m-backup"
            Component.onCompleted: {
                archivehandler.getArchivePodData()
            }
            onFilterChanged: {
                podqast.hfilter = filter
                archivePostModel.clear()
                archivehandler.getArchivePodData()
                archivehandler.getArchiveEntries(podqast.hfilter)
            }
        }

        SilicaListView {
            id: archivepostlist
            anchors.top: podselectgrid.bottom
            // anchors.top: archivetitle.bottom
            width: parent.width
            height: page.height - pdp.height - archivetitle.height - podselectgrid.height
            section.property: 'asection'
            section.delegate: SectionHeader {
                text: section
                horizontalAlignment: Text.AlignRight
            }
            ViewPlaceholder {
                enabled: archivePostModel.count == 0
                text: qsTr("Rendering")
                hintText: qsTr("Collecting Posts")
                verticalOffset: - archivetitle.height - podselectgrid.height
            }

            model: ListModel {
                id: archivePostModel
            }
            delegate: ArchivePostListItem { }
        }

        PlayDockedPanel { id: pdp }
    }
}
