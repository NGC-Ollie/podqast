import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    property var title
    property var description
    property int margin: 30

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    SilicaFlickable {
        anchors.fill: parent
        VerticalScrollDecorator { flickable: flickable }
        contentWidth: detailcolumn.width
        contentHeight: detailcolumn.height + detailcolumn.spacing

        Column {
            id: detailcolumn
            width: page.width
            spacing: Theme.paddingMedium

            PageHeader {
                title: page.title
            }
            Label {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                text: description
                wrapMode: Text.WordWrap
                textFormat: Text.AutoText
                linkColor: Theme.highlightColor
                onLinkActivated: Qt.openUrlExternally(link)
            }
        }
    }
}
