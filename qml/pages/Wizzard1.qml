import QtQuick 2.0
import Sailfish.Silica 1.0

Dialog {
    id: page
    property string abouttext
    property int margin: Theme.paddingMedium

    onAccepted: {
        pageStack.clear()
        pageStack.push(Qt.resolvedUrl("Wizzard2.qml"))
    }

    SilicaFlickable {
        anchors.fill: parent
        contentWidth: parent.width
        contentHeight: wizz.height + wizz.spacing + about.height + about.spacing
        property int refreshTime

        VerticalScrollDecorator { }
        function get_about() {
            var xhr = new XMLHttpRequest
            xhr.open("GET", qsTr("../data/about.txt"))
            xhr.onreadystatechange = function() {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    page.abouttext = xhr.responseText;
                    // use file contents as required
                }
            };
            xhr.send();
        }

        Component.onCompleted: get_about()


        Column {
            id: wizz
            width: parent.width

            DialogHeader {
                acceptText: qsTr("Next")
            }
        }
        SilicaFlickable {
            id: about
            anchors.top: wizz.bottom
            width: parent.width

            VerticalScrollDecorator { }

            // contentWidth: aboutlabel.width
            Column {
                id: bannercolumn
                width: parent.width
                height: parent.width / 2
                Image {
                    source: "../../images/banner.png"
                    width: parent.width
                    height: parent.height
                }
            }

            Column {
                id: aboutcolumn
                anchors.top: bannercolumn.bottom
                width: parent.width
                spacing: margin
                Label {
                    id: aboutlabel
                    text: page.abouttext
                    font.pixelSize: Theme.fontSizeSmall
                    anchors {
                        left: parent.left
                        right: parent.right
                        margins: margin
                    }
                    wrapMode: Text.WordWrap
                    textFormat: Text.AutoText
                    linkColor: Theme.highlightColor
                    onLinkActivated: Qt.openUrlExternally(link)
                }
            }
        }
    }
}
