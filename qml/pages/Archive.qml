import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    SilicaFlickable {
        anchors.fill: parent

        VerticalScrollDecorator { }

        AppMenu { thispage: "Archive" }
        PrefAboutMenu {}

        // Tell SilicaFlickable the height of its content.
        contentHeight: page.height

        Component.onCompleted: {
            feedparserhandler.getPodcasts()
        }

        Column {
            property bool refreshing: false
            id: archivetitle

            width: page.width

            spacing: Theme.paddingLarge
            PageHeader {
                Column {
                    id: refreshcol
                    property real refreshprogress: 1.0
                    anchors.verticalCenter: parent.verticalCenter
                    IconButton {
                        id: refreshicon
                        enabled: refreshcol.refreshprogress == 1.0 && ! archivetitle.refreshing
                        icon.source: "image://theme/icon-m-refresh"
                        onClicked: {
                            // refresh()
                            if (wifiConnected || doMobileDownConf.value ) {
                                archivetitle.refreshing = true
                                feedparserhandler.refreshPodcasts()
                            }
                        }
                        ProgressCircle {
                            borderWidth: 2
                            width: parent.width * 0.8
                            height: parent.height * 0.8
                            visible: refreshcol.refreshprogress != 1.0
                            anchors.centerIn: refreshicon
                            progressValue: refreshcol.refreshprogress
                        }
                    }
                }

                title: qsTr("Library")
            }
        }

        Connections {
            target: feedparserhandler

            onNewPodcasts: {
                archivetitle.refreshing = false
            }
            onRefreshProgress: {
                console.log("Refresh progress" + progress)
                refreshcol.refreshprogress = progress
            }
        }


        SilicaListView {
            id: archivepostlist
            anchors.top: archivetitle.bottom
            width: parent.width
            height: Theme.itemSizeLarge * 2

            VerticalScrollDecorator { }

            // height: page.height - pdp.height - archivetitle.height
            model: ListModel {
                id: submodel
                ListElement {
                    title: qsTr("History")
                    image: "image://theme/icon-m-backup"
                    thepage: "History.qml"
                }
                ListElement {
                    title: qsTr("Favorites")
                    image: "image://theme/icon-m-favorite-selected"
                    thepage: "Favorites.qml"
                }
            }
            delegate: ListItem {
                width: ListView.view.width
                contentHeight: Theme.itemSizeMedium
                onClicked: pageStack.push(Qt.resolvedUrl(thepage))
                Column {
                    id: iconcol
                    anchors.left: parent.left
                    Image {
                        source: image
                    }
                }
                Column {
                    id: labelcol
                    anchors.left: iconcol.right
                    anchors.right: parent.right
                    Label {
                        text: title
                        padding: 10
                    }
                }
            }
        }
        SilicaListView {
            id: podcasts
            anchors.top: archivepostlist.bottom
            width: parent.width
            height: page.height - pdp.height - archivepostlist.height - archivetitle.height
            Connections {
                target: feedparserhandler
                onPodcastsList: {
                    console.log("Data length: " + pcdata.length)
                    podcastsModel.clear()
                    for (var i = 0; i < pcdata.length; i++) {
                        console.log(pcdata[i])
                        podcastsModel.append(pcdata[i]);
                    }
                }
            }

            ViewPlaceholder {
                enabled: podcastsModel.count == 0
                text: qsTr("Rendering")
                hintText: qsTr("Collecting Podcasts")
                verticalOffset: - archivepostlist.height - archivetitle.height
            }

            model: ListModel {
                id: podcastsModel
            }
            delegate: PodcastItem { }
        }

        PlayDockedPanel { id: pdp }
    }
}
