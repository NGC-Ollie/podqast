import QtQuick 2.0
import Sailfish.Silica 1.0

Dialog {
    SilicaFlickable {
        anchors.fill: parent
        contentWidth: parent.width
        contentHeight: downloadConf.height + downloadConf.spacing + dialogheader.height + dialogheader.spacing

        property int refreshTime

        VerticalScrollDecorator { }

        DialogHeader {
            id: dialogheader
            // title: qsTr("Settings")
        }

        Column {
            anchors.top: dialogheader.bottom
            id: downloadConf
            width: parent.width
            spacing: Theme.paddingMedium

            Label {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }

                text: qsTr("Global Podcast Settings")
                font.pixelSize: Theme.fontSizeMedium
                color: Theme.highlightColor
            }

            ComboBox {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                id: moveTo
                width: parent.width
                label: qsTr("Move new post to")
                menu: ContextMenu {
                    MenuItem { text: qsTr("Inbox") }
                    MenuItem { text: qsTr("Top of Playlist") }
                    MenuItem { text: qsTr("Bottom of Playlist") }
                    MenuItem { text: qsTr("Archive") }
                }
            }
            Slider {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                id: autoLimit
                width: parent.width
                label: qsTr("Automatic post limit")
                minimumValue: 0
                maximumValue: 10
                handleVisible: true
                valueText: value == 0 ? "All" : value
                stepSize: 1
            }
            Slider {
                id: globalPlayrate
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                width: parent.width
                label: qsTr("Audio playrate")
                minimumValue: 0.85
                maximumValue: 2.0
                handleVisible: true
                valueText: "1:" + value
                stepSize: 0.15
            }

            Label {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                text: qsTr("Download/Streaming")
                font.pixelSize: Theme.fontSizeMedium
                color: Theme.highlightColor
            }
            anchors {
                left: parent.left
                right: parent.right
                margins: Theme.paddingMedium
            }
            TextSwitch {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }

                id: doDownload
                text: qsTr("Download Playlist Posts")
            }
//            TextSwitch {
//                id: doDownloadFavs
//                text: qsTr("Download Favorites")
//            }
            TextSwitch {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                id: doMobileDown
                text: qsTr("Download on Mobile")
            }
//            Slider {
//                id: downLimit
//                width: parent.width
//                label: qsTr("Download limit")
//                minimumValue: 0
//                maximumValue: 2000
//                handleVisible: true
//                valueText: value ===0 ? "No limit" : value + "MB"
//                stepSize: 100
//            }
//            TextSwitch {
//                id: warnMobileStream
//                text: qsTr("Warn if streaming from Mobile")
//            }
//            TextSwitch {
//                id: saveOnSD
//                text: qsTr("Save data on SD card")
//            }
            Label {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                text: qsTr("System")
                font.pixelSize: Theme.fontSizeMedium
                color: Theme.highlightColor
            }
            TextSwitch {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                id: dataTrackable
                text: qsTr("Audio viewable in system")
            }
            Label {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                text: qsTr("Development")
                font.pixelSize: Theme.fontSizeMedium
                color: Theme.highlightColor
            }
            TextSwitch {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                id: experimentalFlag
                text: qsTr("Experimental features")
            }

//            Label {
//                text: qsTr("Privacy")
//                font.pixelSize: Theme.fontSizeMedium
//                color: Theme.highlightColor
//            }
//            TextSwitch {
//                id: useGpodder
//                text: qsTr("Use gpodder.net search")
//            }
        }
    }
    onOpened: {
        // useGpodder.checked = useGpodderConf.value
        dataTrackable.checked = dataTrackableConf.value
        // saveOnSD.checked = saveOnSDCardConf.value
        doDownload.checked = doDownloadConf.value
        doMobileDown.checked = doMobileDownConf.value
        moveTo.currentIndex = moveToConf.value
        autoLimit.value = autoLimitConf.value
        globalPlayrate.value = globalPlayrateConf.value
        experimentalFlag.checked = experimentalConf.value
        // downLimit.value = downLimitConf.value
        // refreshTime = refreshTimeConf.value
    }
    onAccepted: {
        // useGpodderConf.value = useGpodder.checked
        dataTrackableConf.value = dataTrackable.checked

        feedparserhandler.nomedia(dataTrackableConf.value)

        // saveOnSDCardConf.value = saveOnSD.checked
        doDownloadConf.value = doDownload.checked
        doMobileDownConf.value = doMobileDown.checked
        moveToConf.value = moveTo.currentIndex
        autoLimitConf.value = autoLimit.value
        globalPlayrateConf.value = globalPlayrate.value
        experimentalConf.value = experimentalFlag.checked
        // downLimitConf.value = downLimit.value
        // refreshTimeConf.value = refreshTime
    }
}
