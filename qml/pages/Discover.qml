import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4
import "../components"

Page {
    id: page
    allowedOrientations: Orientation.All
    property int margin: 16

    SilicaFlickable {
        id: mainflick
        anchors.fill: parent
        anchors.left: page.left

        AppMenu { thispage: "Discover" }
        PrefAboutMenu {}

        Row {
            id: discovertitle
            width: page.width
            Column {
                id: column
                width: page.width
                spacing: Theme.paddingLarge

                PageHeader {
                    title: qsTr("Discover")
                }
            }
        }

        SilicaListView {
            // anchors.top: toppodgrid.bottom
            anchors.top: discovertitle.bottom
            width: parent.width
            height: 600
            property bool experimental: experimentalConf.value

            model: ListModel {
                id: menumodel
                ListElement {
                    title: qsTr("Search...")
                    nextpage: "DiscoverSearch.qml"
               }
                ListElement {
                    title: qsTr("Tags...")
                    nextpage: "DiscoverTags.qml"
                }
                ListElement {
                    title: qsTr("Url...")
                    nextpage: "DiscoverUrl.qml"
                }
                ListElement {
                    title: qsTr("Import...")
                    nextpage: "DiscoverImport.qml"
                }
                ListElement {
                    title: qsTr("Export...")
                    nextpage: "DiscoverExport.qml"
                }
            }

            delegate: ListItem {
                onClicked: pageStack.push(Qt.resolvedUrl(nextpage))
                Label {
                    anchors.left: parent.left
                    anchors.leftMargin: margin
                    anchors.verticalCenter: parent.verticalCenter
                    text: title
                }
                IconButton {
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    icon.source: "image://theme/icon-m-right"
                    onClicked: pageStack.push(Qt.resolvedUrl(nextpage))
                }
            }
        }
        PlayDockedPanel { }
    }
}
