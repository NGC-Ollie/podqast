import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4

Python {
    id: gpodderhandler

    signal podSearch(var podcasts)
    signal topTags(var tagslist)
    signal podcastList(var pclist)

    Component.onCompleted: {
        setHandler("podsearch", podSearch)
        setHandler("toptags", topTags)
        setHandler("podcastlist", podcastList)

        addImportPath(Qt.resolvedUrl('.'));
        importModule('GpodderNet', function () {
            console.log('GpodderNet is now imported')
        })
    }
    function getTopList() {
        call("GpodderNet.gpoddernet.gettoplist", function() {});
        // call("GpodderNet.get_toplist", function() {});
    }
    function doSearch(query) {
        call("GpodderNet.gpoddernet.searchpods", [query], function() {});
        // call("GpodderNet.search_pods", [query], function() {});
    }
    function getTags() {
        call("GpodderNet.gpoddernet.gettags", function() {});
        // call("GpodderNet.get_toptags", function() {});
    }
    function getPodcasts(tagname) {
        call("GpodderNet.gpoddernet.getpcbytagname", [tagname], function() {});
    }
}
