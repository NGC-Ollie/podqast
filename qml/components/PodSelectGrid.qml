import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Thumbnailer 1.0

SilicaListView {
    id: gridview
    width: parent.width
    height: Theme.itemSizeLarge
    orientation: ListView.Horizontal

    property string homeicon: "image://theme/icon-m-folder"
    property var filter: podqast.hfilter

    // HorizontalScrollDecorator {}

    Connections {
        target: archivehandler
        onArchivePodList: {
            console.log("onArchivePodList")
            podselectmodel.clear()

            var keys = Object.keys(data)

            console.log("keys length " + keys.length)
            console.log(data)
            // gridview.width = keys.length * Theme.itemSizeMedium
            gridview.contentWidth = keys.length * Theme.itemSizeMedium

            var thecount = 0

            for (var i = 0; i < keys.length; i++ ) {
                console.log(keys[i])
                console.log(data[keys[i]].count)
                console.log(data[keys[i]].logo_url)
                thecount += data[keys[i]].count
                data[keys[i]].podurl = keys[i]
                data[keys[i]].is_enabled = (keys[i] === filter)

                podselectmodel.append(data[keys[i]])
            }
            podselectmodel.insert(0, {
                                       podurl: "home",
                                       logo_url : homeicon,
                                       count: thecount,
                                       is_enabled: filter === "home"
                                  })
//            podselectmodel.insert(0, {
//                                      podurl: "none",
//                                      logo_url: "image://theme/icon-m-search",
//                                      count: 0,
//                                      is_enabled: false
//                                  })
        }
    }

    model: ListModel {
        id: podselectmodel
    }

    delegate: ListItem {
        width: Theme.iconSizeLarge
        height: Theme.iconSizeLarge
        highlighted: is_enabled
        Image {
            source: logo_url
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            width: Theme.iconSizeMedium
            height: Theme.iconSizeMedium
            // opacity: is_enabled ? 1.0 : 0.8
            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: parent.bottom
                text: count
                font.pixelSize: Theme.fontSizeExtraSmall
                font.bold: true
                color: Theme.highlightColor
                z: 2
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    console.log(podurl)
                    if (podurl != "none") {
                        filter = podurl
                    }
                }
            }
        }
    }
}
