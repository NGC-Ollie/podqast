import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Thumbnailer 1.0

ListItem {
    id: podcastItem
    width: ListView.view.width
    contentHeight: Theme.itemSizeLarge
    // onClicked: openMenu()
    onClicked: pageStack.push(Qt.resolvedUrl("../pages/PodpostList.qml"), { url: url, title: title })

    menu: Component {
        IconContextMenu {
            id: podcastContextMenu
            IconMenuItem {
                text: qsTr('Refresh')
                icon.source: 'image://theme/icon-m-refresh'
                onClicked: {
                    closeMenu()
                    feedparserhandler.refreshPodcast(url)
                }
            }
            IconMenuItem {
                text: qsTr('Settings')
                icon.source: 'image://theme/icon-m-developer-mode'
                onClicked: {
                    closeMenu()
                    pageStack.push(Qt.resolvedUrl("../pages/PodcastSettings.qml"), { podtitle: title, url: url })
                }
            }
            IconMenuItem {
                text: qsTr('Delete')
                icon.source: 'image://theme/icon-m-delete'
                onClicked: {
                    var fph = feedparserhandler
                    var theurl = url
                    closeMenu()
                    Remorse.itemAction(podcastItem, qsTr("Deleting Podcast"), function() {
                        fph.deletePodcast(theurl)
                    })
                }
            }
        }
    }
    Thumbnail {
        id: listicon

        anchors.left: parent.left
        width: Theme.iconSizeLarge
        height: Theme.iconSizeLarge
        sourceSize.width: Theme.iconSizeLarge
        sourceSize.height: Theme.iconSizeLarge
        anchors.margins: Theme.paddingMedium
        anchors.verticalCenter: parent.verticalCenter
        source: logo_url == "" ? "../../images/podcast.png" : logo_url
    }

    Label {
        id: titlelabel
        anchors.margins: Theme.paddingMedium
        anchors.left: listicon.right
        anchors.right: listarrow.left
        height: parent.height
        text: title
        font.pixelSize: Theme.fontSizeSmall
        font.bold: true
        wrapMode: Text.WordWrap
    }
    IconButton {
        id: listarrow
        height: parent.height
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        icon.source: "image://theme/icon-m-right"
        onClicked: {
            pageStack.push(Qt.resolvedUrl("../pages/PodpostList.qml"), { url: url, title: title })
        }
    }
}
