import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Thumbnailer 1.0

ListItem {
    width: ListView.view.width
    contentHeight: Theme.itemSizeExtraLarge * 1.3
    // onClicked: openMenu()
    onClicked: pageStack.push(Qt.resolvedUrl("../pages/PostDescription.qml"), { title: title, detail: detail,
                              length: length, date: fdate, duration: duration, href: link})

    onPressAndHold: console.log("press and hold")
    clip: true
    property bool isfavorite: favorite

    menu: IconContextMenu {
        id: contextMenu
        IconMenuItem {
            text: 'Play'
            icon.source: "image://theme/icon-m-" + (id === podqast.firstid && podqast.playing ? "pause" : "play")
            // icon.source: 'image://theme/icon-m-play'
            onClicked: {
                if (id === podqast.firstid) {
                    podqast.playing = ! podqast.playing;
                    if (podqast.playing) {
                        playerHandler.play()
                    } else {
                        playerHandler.pause()
                    }
                } else {
                    queuehandler.queueInsertTop(id);
                    playicon=logo_url;
                    playtext=title;
                }
                closeMenu();
            }
        }
        IconMenuItem {
            text: 'QueueTop'
            icon.source: 'image://theme/icon-m-up'
            onClicked: {
                queuehandler.queueInsertNext(id);
                closeMenu()
            }
        }
        IconMenuItem {
            text: 'QueueBottom'
            icon.source: 'image://theme/icon-m-down'
            onClicked: {
                queuehandler.queueInsertBottom(id);
                closeMenu()
            }
        }
        IconMenuItem {
            text: 'Favorite'
            icon.source: 'image://theme/icon-m-favorite' + (isfavorite ? "-selected" : "")
            onClicked: {
                favoritehandler.toggleFavorite(id)
            }
            Connections {
                target: favoritehandler
                onFavoriteToggled: {
                    if (id == podpost) {
                        isfavorite = favstate
                    }
                }
            }
        }
    }

    Rectangle {
        id: charrect
        anchors.fill: listicon
        visible: logo_url == ""
        color: Theme.rgba(Theme.highlightColor, 0.5)

        clip: true

        Label {
            anchors.centerIn: parent

            font.pixelSize: parent.height * 0.8
            text: title[0]
            color: Theme.highlightColor
        }
    }
    Thumbnail {
        id: listicon
        property int percentage: dlperc
        anchors.leftMargin: Theme.paddingMedium

        MouseArea {
            anchors.fill: parent
            onClicked: {
                 pageStack.push(Qt.resolvedUrl("../pages/PodpostList.qml"), { url: url })
            }
        }
        anchors.left: parent.left
        width: Theme.iconSizeLarge
        height: Theme.iconSizeLarge
        sourceSize.width: Theme.iconSizeLarge
        sourceSize.height: Theme.iconSizeLarge
        anchors.verticalCenter: parent.verticalCenter
        source: logo_url == "" ? "../../images/podcast.png" : logo_url
        Rectangle {
            id: dlstatus
            visible: listicon.percentage > 0 && listicon.percentage < 100
            anchors.right: parent.right
            height: parent.height
            width: (100 - listicon.percentage) * parent.width / 100
            color: "black"
            opacity: 0.7
            z: 0
        }
        Connections {
            target: queuehandler
            onDownloading: {
                if (dlid == id) {
                    listicon.percentage = percent
                }
            }
        }
        Image {
            source: "image://theme/icon-lock-application-update"
            visible: listicon.percentage == 100
            z: 1
            width: parent.width / 4
            height: parent.height / 4
            anchors.bottom: parent.bottom
            anchors.right: parent.right
        }
        Image {
            source: "image://theme/icon-lock-voicemail"
            visible: id === podqast.firstid
            z: 1
            width: parent.width / 4
            height: parent.height / 4
            anchors.top: parent.top
            anchors.left: parent.left
        }
    }


    Column {
        anchors.left: listicon.right
        anchors.right: listarrow.left
        anchors.margins: Theme.paddingMedium
        height: parent.height
        clip: true

        Label {
            id: queueTitlelabel
            width: parent.width
            text: title
            font.pixelSize: Theme.fontSizeExtraSmall
            font.bold: true
            wrapMode: Text.WordWrap
            padding: 5
        }
        Label {
            id: queueTimelabel
            width: parent.width
            font.pixelSize: Theme.fontSizeTiny
            wrapMode: Text.WordWrap
            textFormat: Text.StyledText
            text: timestring == "" ? description : timestring + " · " + description
            padding: 5
            Connections {
                target: queuehandler
                onHrtime: {
                    if (dlid == id) {
                        queueTimelabel.text = tmstring + " · " + description
                    }
                }
            }
        }
    }

    IconButton {
        id: listarrow
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        icon.source: "image://theme/icon-m-right"
        onClicked: {
            // feedparserhandler.renderHtml(detail)
            pageStack.push(Qt.resolvedUrl("../pages/PostDescription.qml"), { title: title, detail: detail,
                           length: length, date: fdate, duration: duration, href: link})
        }
    }
}
