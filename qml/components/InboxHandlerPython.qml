import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4

Python {
    id: inboxhandler

    signal createInboxList(var data)
    signal getInboxPosts()

    Component.onCompleted: {
        setHandler("createInboxList", createInboxList)
        setHandler("getInboxPosts", getInboxPosts)

        addImportPath(Qt.resolvedUrl('.'));
        importModule('InboxHandler', function () {
            console.log('InboxHandler is now imported')
        })
    }

    function getInboxEntries(podurl) {
        if (podurl === "home") {
            // call("InboxHandler.inboxhandler.getinboxposts", function() {});
            call("InboxHandler.get_inbox_posts", function() {});
        } else {
            // call("InboxHandler.inboxhandler.getinboxposts", [podurl], function() {});
            call("InboxHandler.get_inbox_posts", [podurl], function() {});
        }

    }
    function moveQueueTop(podpost) {
        call("InboxHandler.inboxhandler.movequeuetop", [podpost], function() {});
        // call("InboxHandler.move_queue_top", [podpost], function() {});
    }
    function moveQueueNext(podpost) {
        call("InboxHandler.inboxhandler.movequeuenext", [podpost], function() {});
        // call("InboxHandler.move_queue_next", [podpost], function() {});
    }
    function moveQueueBottom(podpost) {
        call("InboxHandler.inboxhandler.movequeuebottom", [podpost], function() {});
        // call("InboxHandler.move_queue_bottom", [podpost], function() {});
    }
    function moveArchive(podpost) {
        call("InboxHandler.inboxhandler.movearchive", [podpost], function() {});
        // call("InboxHandler.move_archive", [podpost], function() {});
    }
    function moveAllArchive(podpost) {
        // call("InboxHandler.inboxhandler.moveallarchive", function() {});
        call("InboxHandler.move_all_archive", function() {});
    }
    function getInboxPodData() {
        // call("InboxHandler.inboxhandler.getinboxpoddata", function() {});
        call("InboxHandler.get_inbox_pod_data", function() {});
    }

}
