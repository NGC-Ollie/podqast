import QtQuick 2.0
import Sailfish.Silica 1.0

PushUpMenu {
    MenuItem {
        text: qsTr("Settings")
        onClicked: pageStack.push(Qt.resolvedUrl("../pages/Settings.qml"))
    }
    MenuItem {
        text: qsTr("About")
        onClicked: pageStack.push(Qt.resolvedUrl("../pages/About.qml"))
    }
}
