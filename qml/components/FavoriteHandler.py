#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pyotherside
import threading
import sys

sys.path.append("/usr/share/harbour-podqast/python")

from podcast.factory import Factory

def toggle_favorite(podpost):
    """
    Return a list of all archive posts
    """

    post = Factory().get_podpost(podpost)
    if not post:
        return

    pyotherside.send('favoriteToggled', podpost, post.toggle_fav())

def get_queue_favorites(podurl=None):
    """
    Return a list of all favorites posts from queue
    """

    from podcast.queue import QueueFactory

    posts = []

    queue = QueueFactory().get_queue()
    for post in queue.get_podposts():
        p = Factory().get_podpost(post)
        try:
            if p.favorite:
                if podurl:
                    if p.podurl == podurl:
                        posts.append(p)
                else:
                    posts.append(p)
        except:
            pass
    return posts

def append_archive_favorites(posts, podurl=None):
    """
    append archive favorites to posts list
    """

    from podcast.archive import ArchiveFactory
    archive = ArchiveFactory().get_archive()
    
    for post in archive.get_podposts():
        p = Factory().get_podpost(post)
        try:
            if p.favorite:
                if p not in posts:
                    if podurl:
                        if p.podurl == podurl:
                            posts.append(p)
                    else:
                        posts.append(p)
        except:
            pass

    return posts

def get_favorites(podurl=None):
    """
    Test
    """

    posts = get_queue_favorites(podurl)
    posts = append_archive_favorites(posts, podurl=podurl)
    postsdata = []
    for post in posts:
        postsdata.append(post.get_data())
        
    postsdata.sort(key = lambda r: r["adate"], reverse=True )
    pyotherside.send("favoriteList", postsdata)

def get_fav_pod_data():
    """
    """

    entries = {}

    posts = get_queue_favorites()
    posts = append_archive_favorites(posts)
    for p in posts:
        if p.podurl in entries:
            entries[p.podurl]["count"] += 1
        else:
            if p.logo_url:
                logo_url = p.logo_url
            else:
                logo_url = "../../images/podcast.png"
            entries[p.podurl] = { "count": 1,
                                 "logo_url": logo_url }
            
    pyotherside.send('archivePodList', entries)

class FavoriteHandler:
    def __init__(self):
        self.bgthread = threading.Thread()
        self.bgthread.start()
        self.bgthread2 = threading.Thread()
        self.bgthread2.start()

    def togglefavorite(self, podpost):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=toggle_favorite, args=[podpost])
        self.bgthread.start()

    def getfavorites(self, podurl=None):
        if self.bgthread2.is_alive():
            return
        if podurl:
            self.bgthread2 = threading.Thread(target=get_favorites, args=[podurl])
        else:
            self.bgthread2 = threading.Thread(target=get_favorites)
        self.bgthread2.start()
            
    def getfavpoddata(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_fav_pod_data)
        self.bgthread.start()

favoritehandler = FavoriteHandler()
