#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pyotherside
import threading
import sys
import os
import time
import tarfile

sys.path.append("/usr/share/harbour-podqast/python")

from podcast.podcast import PodcastFactory, Podcast
from podcast.podcastlist import PodcastListFactory
from podcast.queue import QueueFactory
from podcast.archive import ArchiveFactory
from podcast.inbox import InboxFactory
from podcast.factory import Factory
from podcast.util import create_opml

from cacher import memcacher

cachevar = {}

def get_feedinfo(url):
    podcast = PodcastFactory().get_podcast(url)
    pyotherside.send('feedinfo', podcast.feedinfo())

def get_alternatives(url):
    podcast = PodcastFactory().get_podcast(url)
    pyotherside.send('alternatives', podcast.get_alt_feeds())

def get_first_entry(url):
    """
    Get first Podcast entry
    """

    podcast = PodcastFactory().get_podcast(url)
    entry = podcast.get_first_entry()
    data = entry.get_data()
    pyotherside.send('fevent', data)

def get_last_entry(url):

    podcast = PodcastFactory().get_podcast(url)
    entry = podcast.get_last_entry()
    data = entry.get_data()
    pyotherside.send('levent', data)

def get_entries(url):
    """
    Get all podposts
    """

    podcast = PodcastFactory().get_podcast(url)

    feeds = []

    for feed_data in podcast.get_entries():
        feeds.append(feed_data)

    try:
        link = podcast.link
    except:
        podcast.link = podcast.feed.feed.link
        
    feeds.sort(key = lambda r: r["date"], reverse=True)
    pyotherside.send("createPodpostsList", feeds, podcast.title, podcast.link)

def subscribe_podcast(url):
    """
    Subscribe a podcast
    """

    podcast_list = PodcastListFactory().get_podcast_list()
    podcast_list.add(url)
    pyotherside.send("subscribed", url)

def get_podcasts():
    """
    Get Podcast List
    """

    podcasts = []
    pclist = PodcastListFactory().get_podcast_list()

    for podcast in pclist.get_podcasts():
        pc = PodcastFactory().get_podcast(podcast)
        podcasts.append(pc.feedinfo())

    pyotherside.send('podcastslist', podcasts)

def delete_podcast(url):
    """
    Delete a podcast from list
    """

    pclist = PodcastListFactory().get_podcast_list().delete_podcast(url)
    get_podcasts()

def movePost(post):
    """
    Do move the object into the right place
    Returns page for Notification
    """

    page = "Inbox"
    if post["move"] == 0:
        InboxFactory().get_inbox().insert(post["id"])
        page = "Inbox"
    elif post["move"] == 1:
        QueueFactory().get_queue().insert_next(post["id"])
        page = "Queue"
    elif post["move"] == 2:
        QueueFactory().get_queue().insert_bottom(post["id"])
        page = "Queue"
    elif post["move"] == 3:
        ArchiveFactory().get_archive().insert(post["id"])
        page = "History"

    return page

def refresh_podcast(url, moveto, download, limit=0):
    """
    Refresh one podcast
    """

    podcast = PodcastFactory().get_podcast(url)
    posts = []

    for post in podcast.refresh(moveto, limit):
        page = movePost(post)        
        posts.append(post["id"])
        pyotherside.send("updatesNotification", post["pctitle"], post["pstitle"], page)

    pyotherside.send("newPodcasts", posts)

def refresh_podcasts(moveto, download, limit=0):
    """
    Refresh all podcasts
    """

    podcast_list = PodcastListFactory().get_podcast_list()
    
    posts = []

    for post in podcast_list.refresh(moveto, limit):
        page = movePost(post)
        posts.append(post["id"])
        pyotherside.send("updatesNotification", post["pctitle"], post["pstitle"], page)
        # pyotherside.send("refreshProgress", count / pllen)
        
    pyotherside.send("newPodcasts", posts)
    
def get_podcast_params(url):
    """
    Get Parameter set of podcast
    """

    podcast = PodcastFactory().get_podcast(url)
    pyotherside.send("podcastParams", podcast.get_params())

def set_podcast_params(url, params):
    """
    Put parameter set to podcast
    """

    podcast = PodcastFactory().get_podcast(url)
    podcast.set_params(params)

def render_html(data):
    """
    Render a temporary page
    """

    storepath = Factory().iconpath
    htmlfile = os.path.join(storepath, 'page.html')
    with open(htmlfile, 'wb') as h:
        h.write('<html><body>'.encode())
        h.write(data.encode())
        h.write('</body></html'.encode())
    pyotherside.send("htmlfile", htmlfile)

def nomedia(doset):
    """
    Set nomedia in config root
    """

    Factory().nomedia(doset)

def import_opml(opmlfile):
    """
    Import podcasts from opmlfile
    """

    pclist = PodcastListFactory().get_podcast_list()
    imported = pclist.import_opml(opmlfile)
    if imported > 0:
        pyotherside.send("opmlimported", imported)

def import_gpodder():
    """
    Import podcasts from opmlfile
    """

    pclist = PodcastListFactory().get_podcast_list()
    imported = pclist.import_gpodder()
    if imported > 0:
        pyotherside.send("opmlimported", imported)

def do_backup():
    """
    Create a backup tarball of store and icons
    """

    rootpath = Factory().data_home
    homedir = os.path.expanduser('~')
    os.chdir(homedir)
    filename = "podqast-%s.tar.gz" % time.strftime('%Y%m%d%H%M')
    tarfilename = os.path.join(homedir, filename)

    try:
        tar = tarfile.open(tarfilename, "w:gz")
        tar.add(os.path.join(rootpath, 'icons'), arcname='./icons')
        tar.add(os.path.join(rootpath, 'store'), arcname='./store')
        tar.close()
    except:
        pyotherside.send("error", "Backup failed")
        return

    pyotherside.send("backupDone", tarfilename)

def write_opml():
    """
    Create opml file
    """

    podcasts = []
    pclist = PodcastListFactory().get_podcast_list()
    for podcast in pclist.get_podcasts():
        pc = PodcastFactory().get_podcast(podcast)
        podcasts.append({
            'name': pc.title,
            'xml_url': pc.url,
            'html_url': pc.link
            })

    rootpath = Factory().data_home
    homedir = os.path.expanduser('~')
    os.chdir(homedir)
    filename = "podqast-%s.opml" % time.strftime('%Y%m%d%H%M')
    opmlfilename = os.path.join(homedir, filename)

    create_opml(opmlfilename, podcasts)
    
class FeedParser:
    def __init__(self):
        self.bgthread = threading.Thread()
        self.bgthread.start()

        self.bgthread1 = threading.Thread()
        self.bgthread1.start()
        
    def getfeedinfo(self, theurl):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_feedinfo, args=[theurl])
        self.bgthread.start()

    def getalternatives(self, theurl):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_alternatives, args=[theurl])
        self.bgthread.start()

    def getfirstentry(self, theurl):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_first_entry, args=[theurl])
        self.bgthread.start()

    def getentries(self, theurl):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_entries, args=[theurl])
        self.bgthread.start()
        
    def getlastentry(self, theurl):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_last_entry, args=[theurl])
        self.bgthread.start()

    def getpodcasts(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_podcasts)
        self.bgthread.start()

    def subscribepodcast(self, theurl):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=subscribe_podcast, args=[theurl])
        self.bgthread.start()

    def deletepodcast(self, theurl):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=delete_podcast, args=[theurl])
        self.bgthread.start()

    def refreshpodcast(self, theurl, moveto, download):
        if self.bgthread.is_alive():
            return
        self.bgthread1 = threading.Thread(target=refresh_podcast,
                                         args=[theurl, moveto, download])
        self.bgthread1.start()

    def refreshpodcasts(self, moveto, download):
        if self.bgthread.is_alive():
            return
        self.bgthread1 = threading.Thread(target=refresh_podcasts,
                                         args=[moveto, download])
        self.bgthread1.start()

    def getpodcastparams(self, theurl):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_podcast_params, args=[theurl])
        self.bgthread.start()
        
    def setpodcastparams(self, theurl, params):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=set_podcast_params, args=[theurl, params])
        self.bgthread.start()
        
    def renderhtml(self, data):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=render_html, args=[data])
        self.bgthread.start()

    def nomedia(self, doset):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=doset, args=[doset])
        self.bgthread.start()

    def importopml(self, opmlfile):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=import_opml, args=[opmlfile])
        self.bgthread.start()

    def importgpodder(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=import_gpodder)
        self.bgthread.start()

    def dobackup(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=do_backup)
        self.bgthread.start()

    def writeopml(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=write_opml)
        self.bgthread.start()

feedparse = FeedParser()
