#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pyotherside
import threading
import sys

sys.path.append("/usr/share/harbour-podqast/python")

from podcast.inbox import InboxFactory
from podcast.queue import QueueFactory
from podcast.factory import Factory

def get_inbox_posts(podurl=None):
    """
    Return a list of all inbox posts
    """

    entries = []
    inbox = InboxFactory().get_inbox()

    pyotherside.send("len Inbox %d" % len(inbox.podposts))
    
    for post in inbox.get_podposts():
        entry = Factory().get_podpost(post)
        if podurl:
            if entry.podurl == podurl:
                entries.append(entry.get_data())
        else:
            entries.append(entry.get_data())
    
    entries.sort(key = lambda r: r["date"], reverse=True)
    pyotherside.send('createInboxList', entries)

def get_inbox_pod_data():
    """
    """

    entries = {}

    inbox = InboxFactory().get_inbox()

    for post in inbox.get_podposts():
        entry = Factory().get_podpost(post)
        if entry.podurl in entries:
            entries[entry.podurl]["count"] += 1
        else:
            if entry.logo_url:
                logo_url = entry.logo_url
            else:
                logo_url = "../../images/podcast.png"
            entries[entry.podurl] = { "count": 1,
                                      "logo_url": logo_url }

    pyotherside.send('archivePodList', entries)
        
def move_queue_top(podpost):
    """
    Move element to top of queue
    """

    InboxFactory().get_inbox().move_queue_top(podpost)
    # get_inbox_posts()
    pyotherside.send('getInboxPosts')

def move_queue_next(podpost):
    """
    Move element to next queue position
    """

    InboxFactory().get_inbox().move_queue_next(podpost)
    # get_inbox_posts()
    pyotherside.send('getInboxPosts')

def move_queue_bottom(podpost):
    """
    Move element to last queue position
    """

    InboxFactory().get_inbox().move_queue_bottom(podpost)
    # get_inbox_posts()
    pyotherside.send('getInboxPosts')

def move_archive(podpost):
    """
    Move post to archive
    """

    inbox = InboxFactory().get_inbox()
    inbox.move_archive(podpost)
    # get_inbox_posts()
    pyotherside.send('getInboxPosts')

def move_all_archive():
    """
    Move all posts to archive
    """

    inbox = InboxFactory().get_inbox()
    inbox.move_all_archive()
    # get_inbox_posts()
    pyotherside.send('getInboxPosts')

def queue_do_download(id):
    """
    Do the Download of an archive
    """

    queue = QueueFactory().get_queue()
    for perc in queue.download(id):
        pyotherside.send('downloading', id, perc)

class InboxHandler:
    def __init__(self):
        self.bgthread = threading.Thread()
        self.bgthread.start()
        pyotherside.atexit(self.doexit)

    def doexit(self):
        """
        On exit we need to save ourself
        """

        InboxFactory().get_inbox().save()

    def getinboxposts(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_inbox_posts)
        self.bgthread.start()

    def getinboxpoddata(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_inbox_pod_data)
        self.bgthread.start()

    def queuedownload(self, id):
        """
        download audio post
        """
        
        dlthread = threading.Thread(target=queue_do_download,
                                    args=[id])
        dlthread.start()

    def movequeuetop(self, podpost):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=move_queue_top, args=[podpost])
        self.bgthread.start()

    def movequeuenext(self, podpost):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=move_queue_next, args=[podpost])
        self.bgthread.start()

    def movequeuebottom(self, podpost):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=move_queue_bottom, args=[podpost])
        self.bgthread.start()

    def movearchive(self, podpost):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=move_archive, args=[podpost])
        self.bgthread.start()

    def moveallarchive(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=move_all_archive)
        self.bgthread.start()

inboxhandler = InboxHandler()
