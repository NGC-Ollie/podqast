import QtQuick 2.0
import Sailfish.Silica 1.0

ListItem {
    width: ListView.view.width
    height: Theme.itemSizeMedium
    onClicked: pageStack.push(Qt.resolvedUrl("../pages/Podcast.qml"), {
                                              url: url,
                                              title: title,
                                              titlefull: titlefull,
                                              description: description,
                                              website: website,
                                              logo_url: logo_url ? logo_url : "../../images/podcast.png"
                                          })

    Image {
        id: listicon
        property int percentage: 0
        anchors.leftMargin: Theme.paddingMedium
        width: Theme.iconSizeLarge
        height: Theme.iconSizeLarge
        signal failedToLoad
        cache: false
        anchors.left: parent.left
        source: logo_url
        onStatusChanged: {
            if (status == Image.Error) {
                source="../../images/podcast.png"
                failedToLoad()
            }
        }
        BusyIndicator {
            size: BusyIndicatorSize.Small
            anchors.centerIn: listicon
            running: listicon.status != Image.Ready
        }
    }
    Label {
        id: titlelabel
        anchors.margins: 10
        anchors.left: listicon.right
        anchors.right: listarrow.left
        height: parent.height
        text: title
        font.pixelSize: Theme.fontSizeSmall
        font.bold: true
        wrapMode: Text.WordWrap
    }
    IconButton {
        id: listarrow
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        icon.source: "image://theme/icon-m-right"
        onClicked: pageStack.push(Qt.resolvedUrl("../pages/Podcast.qml"), {
                                                  url: url,
                                                  title: title,
                                                  titlefull: titlefull,
                                                  description: description,
                                                  website: website,
                                                  logo_url: logo_url ? logo_url : "../../images/podcast.png"
                                               });
    }
}
