"""
Podpost tests
"""

import sys
sys.path.append("../python")

from podcast.factory import Factory
from podcast.podcast import Podcast
from podcast.podpost import Podpost

def test_podpost_save():
    """
    Test podpost saving
    """

    f1 = Factory()
    
    p = Podcast('https://freakshow.fm/feed/opus/')
    e = p.get_first_entry()
    id1 = e.id
    e.save()
    n = Factory().get_store().get(e.id)
    id2 = n.id
    assert type(n) == Podpost
    assert id1 == id2
    
