#+TITLE: ToDos
#+AUTHOR: Thomas Renard
#+EMAIL: Thomas.Renard@g3la.de
* TODO podpost
  - time stamp when storing a podpost the first time
  - queue.py: if not in self.podposts: get_podpost.set_date -> now in
    top next and bottom
* TODO Podcast subscribing, Setting
  - list subscribed podcasts in Archive
  - list posts in Podcasts.py
* TODO History/Favorites
  - create grid for selecting special podcasts (horizontal scrolling)
* TODO Podcast
  - make it scrollable
  - full list with subsections

