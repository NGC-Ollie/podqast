<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/pages/About.qml" line="9"/>
        <source>../data/about.txt</source>
        <translation>../data/about-de.txt</translation>
    </message>
</context>
<context>
    <name>AppMenu</name>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="9"/>
        <source>Discover</source>
        <translation>Entdecken</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="17"/>
        <source>Library</source>
        <translation>Bibliothek</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="33"/>
        <source>Playlist</source>
        <translation>Playlist</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="25"/>
        <source>Inbox</source>
        <translation>Eingang</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="48"/>
        <source>refreshing: </source>
        <translation>aktualisiere: </translation>
    </message>
</context>
<context>
    <name>Archive</name>
    <message>
        <location filename="../qml/pages/Archive.qml" line="140"/>
        <source>Rendering</source>
        <translation>Zeichne</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="141"/>
        <source>Collecting Podcasts</source>
        <translation>Sammle Podcasts</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="89"/>
        <source>History</source>
        <translation>Verlauf</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="60"/>
        <source>Library</source>
        <translation>Bibliothek</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="94"/>
        <source>Favorites</source>
        <translation>Favoriten</translation>
    </message>
</context>
<context>
    <name>Chapters</name>
    <message>
        <location filename="../qml/pages/Chapters.qml" line="35"/>
        <source>Chapters</source>
        <translation>Kapitel</translation>
    </message>
    <message>
        <location filename="../qml/pages/Chapters.qml" line="94"/>
        <source>No chapters</source>
        <translation>Keine Kapitel</translation>
    </message>
    <message>
        <location filename="../qml/pages/Chapters.qml" line="95"/>
        <source>Rendering chapters</source>
        <translation>Hole Kapitel</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="37"/>
        <source> chapters</source>
        <translation> Kapitel</translation>
    </message>
</context>
<context>
    <name>Discover</name>
    <message>
        <location filename="../qml/pages/Discover.qml" line="28"/>
        <source>Discover</source>
        <translation>Entdecken</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="43"/>
        <source>Search...</source>
        <translation>Suchen...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="47"/>
        <source>Tags...</source>
        <translation>Kategorien...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="51"/>
        <source>Url...</source>
        <translation>Url...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="55"/>
        <source>Import...</source>
        <translation>Importieren...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="59"/>
        <source>Export...</source>
        <translation>Exportieren...</translation>
    </message>
</context>
<context>
    <name>DiscoverExport</name>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="25"/>
        <source>Backup done</source>
        <translation>Backup durchgeführt</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="26"/>
        <location filename="../qml/pages/DiscoverExport.qml" line="27"/>
        <source>Backup done to </source>
        <translation>Backup durchgeführt nach </translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="33"/>
        <source>OPML file saved</source>
        <translation>OPML-File gespeichert</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="34"/>
        <location filename="../qml/pages/DiscoverExport.qml" line="35"/>
        <source>OPML file saved to </source>
        <translation>OPML-File gesichert nach </translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="53"/>
        <source>Export</source>
        <translation>Exportieren</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="64"/>
        <source>Backup</source>
        <translation>Backup</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="67"/>
        <source>Start Backup</source>
        <translation>Starte Backup</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="85"/>
        <source>Save to OPML</source>
        <translation>Speichere als OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="88"/>
        <source>Start OPML export</source>
        <translation>Starte OPML-Export</translation>
    </message>
</context>
<context>
    <name>DiscoverImport</name>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="25"/>
        <source> Podcasts imported</source>
        <translation> Podcasts importiert</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="26"/>
        <location filename="../qml/pages/DiscoverImport.qml" line="27"/>
        <source> Podcasts imported from OPML</source>
        <translation> Podcasts vom OPML importiert</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="46"/>
        <source>Discover by Importing</source>
        <translation>Entdecken durch Importieren</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="59"/>
        <source>Pick an OPML file</source>
        <translation>Wähle eine OPML-Datei</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="60"/>
        <source>OPML file</source>
        <translation>OPML-Datei</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="80"/>
        <source>Import OPML</source>
        <translation>Importiere OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="82"/>
        <source>Import OPML File</source>
        <translation>Importiere OPML-Datei</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="100"/>
        <source>Import from Gpodder</source>
        <translation>Importiere von Gpodder</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="102"/>
        <source>Import Gpodder Database</source>
        <translation>Importiere von Gpodder-Datenbank</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="125"/>
        <source>Please note: Importing does take some time!</source>
        <translation>Beachte: Importieren benötigt etwas Zeit!</translation>
    </message>
</context>
<context>
    <name>DiscoverSearch</name>
    <message>
        <location filename="../qml/pages/DiscoverSearch.qml" line="33"/>
        <source>Discover by Search</source>
        <translation>Entdecken durch Suchen</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverSearch.qml" line="62"/>
        <source>Search</source>
        <translation>Suchen</translation>
    </message>
</context>
<context>
    <name>DiscoverTags</name>
    <message>
        <location filename="../qml/pages/DiscoverTags.qml" line="43"/>
        <source>Discover Tags</source>
        <translation>Entdecke Kategorien</translation>
    </message>
</context>
<context>
    <name>DiscoverUrl</name>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="28"/>
        <source>Discover by URL</source>
        <translation>Entdecke nach URL</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="41"/>
        <source>Enter podcasts&apos; url</source>
        <translation>Gib den Podcast-URL ein</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="49"/>
        <source>Discover</source>
        <translation>Entdecken</translation>
    </message>
</context>
<context>
    <name>Favorites</name>
    <message>
        <location filename="../qml/pages/Favorites.qml" line="46"/>
        <source>Favorites</source>
        <translation>Favoriten</translation>
    </message>
</context>
<context>
    <name>FeedParserPython</name>
    <message>
        <location filename="../qml/components/FeedParserPython.qml" line="169"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
</context>
<context>
    <name>History</name>
    <message>
        <location filename="../qml/pages/History.qml" line="45"/>
        <source>History</source>
        <translation>Verlauf</translation>
    </message>
    <message>
        <location filename="../qml/pages/History.qml" line="80"/>
        <source>Rendering</source>
        <translation>Zeichne</translation>
    </message>
    <message>
        <location filename="../qml/pages/History.qml" line="81"/>
        <source>Collecting Posts</source>
        <translation>Sammle Beiträge</translation>
    </message>
</context>
<context>
    <name>Inbox</name>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="48"/>
        <source>Inbox</source>
        <translation>Eingang</translation>
    </message>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="57"/>
        <source>Moving all posts to archive</source>
        <translation>Lege alle Beiträge ins Archiv</translation>
    </message>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="96"/>
        <source>No new posts</source>
        <translation>Keine neuen Beiträge</translation>
    </message>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="97"/>
        <source>Pull down to Discover new podcasts, get posts from Library, or play the Playlist</source>
        <translation>Menü um neue Podcasts zu Entdecken, hole Beiträge von der Bibliothek oder spiele die Playlist</translation>
    </message>
</context>
<context>
    <name>Player</name>
    <message>
        <location filename="../qml/pages/Player.qml" line="27"/>
        <source>Audio playrate</source>
        <translation>Abspielgeschwindigkeit</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="126"/>
        <source> chapters</source>
        <translation> Kapitel</translation>
    </message>
</context>
<context>
    <name>Podcast</name>
    <message>
        <location filename="../qml/pages/Podcast.qml" line="173"/>
        <source>Subscribe</source>
        <translation>Subscribe</translation>
    </message>
    <message>
        <location filename="../qml/pages/Podcast.qml" line="173"/>
        <source>Configure</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/pages/Podcast.qml" line="210"/>
        <source>Feed alternatives</source>
        <translation>Alternative Feeds</translation>
    </message>
    <message>
        <location filename="../qml/pages/Podcast.qml" line="230"/>
        <source>Latest Post</source>
        <translation>Letzte Sendung</translation>
    </message>
    <message>
        <location filename="../qml/pages/Podcast.qml" line="254"/>
        <source>First Post</source>
        <translation>Erste Sendung</translation>
    </message>
</context>
<context>
    <name>PodcastItem</name>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="16"/>
        <source>Refresh</source>
        <translation>Aktualisieren</translation>
    </message>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="24"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="32"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="38"/>
        <source>Deleting Podcast</source>
        <translation>Lösche Podcast</translation>
    </message>
</context>
<context>
    <name>PodcastSettings</name>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="25"/>
        <source>Posts</source>
        <translation>Beiträge</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="34"/>
        <source>Move new post to</source>
        <translation>Neuen Beitrag</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="36"/>
        <source>Inbox</source>
        <translation>Eingang</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="37"/>
        <source>Top of Playlist</source>
        <translation>an den Anfang der Playlist</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="38"/>
        <source>Bottom of Playlist</source>
        <translation>ans Ende der Playlist</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="39"/>
        <source>Archive</source>
        <translation>in das Archiv</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="46"/>
        <source>Automatic post limit</source>
        <translation>Höchstgrenze automatisierter Beiträge</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="63"/>
        <source>Audio playrate</source>
        <translation>Abspielgeschwindigkeit</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="72"/>
        <source>Podcast</source>
        <translation>Podcast</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="80"/>
        <source>Subscribe</source>
        <translation>Subscribe</translation>
    </message>
</context>
<context>
    <name>Podcastsearch</name>
    <message>
        <location filename="../qml/pages/Podcastsearch.qml" line="31"/>
        <source>Search by Tag...</source>
        <translation>Nach Tag suchen...</translation>
    </message>
</context>
<context>
    <name>PodpostList</name>
    <message>
        <location filename="../qml/pages/PodpostList.qml" line="88"/>
        <source>Rendering</source>
        <translation>Zeichne</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodpostList.qml" line="89"/>
        <source>Creating items</source>
        <translation>Erzeuge Elemente</translation>
    </message>
</context>
<context>
    <name>PostDescription</name>
    <message>
        <location filename="../qml/pages/PostDescription.qml" line="25"/>
        <source>Open in browser</source>
        <translation>Im Browser öffnen</translation>
    </message>
</context>
<context>
    <name>PrefAboutMenu</name>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="6"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="10"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
</context>
<context>
    <name>Queue</name>
    <message>
        <location filename="../qml/pages/Queue.qml" line="68"/>
        <source>Playlist</source>
        <translation>Playlist</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="83"/>
        <source>No posts</source>
        <translation>Keine Beiträge</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="84"/>
        <source>Pull down to Discover new podcasts or get posts from Inbox or Library</source>
        <translation>Menü um neue Podcasts zu Entdecken oder hole Posts vom Eingang oder der Bibliothek</translation>
    </message>
</context>
<context>
    <name>QueuePostListItem</name>
    <message>
        <location filename="../qml/components/QueuePostListItem.qml" line="49"/>
        <source>Favorite</source>
        <translation>Favorit</translation>
    </message>
    <message>
        <location filename="../qml/components/QueuePostListItem.qml" line="65"/>
        <source>Move up</source>
        <translation>Hoch bewegen</translation>
    </message>
    <message>
        <location filename="../qml/components/QueuePostListItem.qml" line="73"/>
        <source>Move down</source>
        <translation>Runter bewegen</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/pages/Settings.qml" line="32"/>
        <source>Global Podcast Settings</source>
        <translation>Globale Podcast-Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="45"/>
        <source>Move new post to</source>
        <translation>Neuen Beitrag</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="47"/>
        <source>Inbox</source>
        <translation>in den Eingang</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="50"/>
        <source>Archive</source>
        <translation>in das Archiv</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="61"/>
        <source>Automatic post limit</source>
        <translation>Höchstgrenze automatisierter Beiträge</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="76"/>
        <source>Audio playrate</source>
        <translation>Abspielgeschwindigkeit</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="90"/>
        <source>Download/Streaming</source>
        <translation>Laden/Stream</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="165"/>
        <source>Development</source>
        <translation>Entwicklung</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="176"/>
        <source>Experimental features</source>
        <translation>Experimentelle Funktionen</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="48"/>
        <source>Top of Playlist</source>
        <translation>an den Anfang der Playlist</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="49"/>
        <source>Bottom of Playlist</source>
        <translation>ans Ende der Playlist</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="107"/>
        <source>Download Playlist Posts</source>
        <translation>Herunterladen von Playlist-Beiträgen</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="120"/>
        <source>Download on Mobile</source>
        <translation>Bei Mobilfunk laden</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="146"/>
        <source>System</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="157"/>
        <source>Audio viewable in system</source>
        <translation>Audio im System sichbar</translation>
    </message>
</context>
<context>
    <name>Wizzard1</name>
    <message>
        <location filename="../qml/pages/Wizzard1.qml" line="23"/>
        <source>../data/about.txt</source>
        <translation>../data/about-de.txt</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard1.qml" line="41"/>
        <source>Next</source>
        <translation>Weiter</translation>
    </message>
</context>
<context>
    <name>Wizzard2</name>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="29"/>
        <source>Let&apos;s start...</source>
        <translation>Loslegen</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="30"/>
        <source>Back to info</source>
        <translation>Zurück zur Information</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="47"/>
        <source> Podcasts imported</source>
        <translation> Podcasts importiert</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="48"/>
        <location filename="../qml/pages/Wizzard2.qml" line="49"/>
        <source> Podcasts imported from OPML</source>
        <translation> Podcasts vom OPML importiert</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="66"/>
        <source>You are now able to import old stuff. You can import from Discover lateron. Please note: Importing does take some time!</source>
        <translation>Du kannst nun alte Sachen importieren. Du kannst das auch später über Entdecken machen. Beachte: Importieren benötigt etwas Zeit</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="95"/>
        <source>Pick an OPML file</source>
        <translation>Wähle eine OPML-Datei</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="96"/>
        <source>OPML file</source>
        <translation>OPML-Datei</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="118"/>
        <source>Import OPML</source>
        <translation>Importiere OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="120"/>
        <source>Import OPML File</source>
        <translation>Importiere OPML-Datei</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="139"/>
        <source>Import from Gpodder</source>
        <translation>Importiere von Gpodder</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="142"/>
        <source>Import Gpodder Database</source>
        <translation>Importiere von Gpodder-Datenbank</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="168"/>
        <source>When you now start you can discover new podcasts. After that you can for example select new posts to play from the Library.</source>
        <translation>Wenn Du weitermachst, kannst Du neue Podcasts aussuchen. Danach kannst Du z. B. neue Posts von der Bibliothek aus aussuchen.</translation>
    </message>
</context>
<context>
    <name>harbour-podqast</name>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="268"/>
        <source>New posts available</source>
        <translation>Neue Beiträge verfügbar</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="269"/>
        <source>Click to view updates</source>
        <translation>Klicken um Aktualisierungen anzusehen</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="270"/>
        <source>New Posts are available. Click to view.</source>
        <translation>Neue Beiträge verfügbar. Klicke zum Betrachten</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="287"/>
        <location filename="../qml/harbour-podqast.qml" line="288"/>
        <source>PodQast message</source>
        <translation>PodQast-Nachricht</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="289"/>
        <source>New PodQast message</source>
        <translation>Neue PodQast-Nachricht</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="315"/>
        <source> Podcasts imported</source>
        <translation> Podcasts importiert</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="316"/>
        <location filename="../qml/harbour-podqast.qml" line="317"/>
        <source> Podcasts imported from OPML</source>
        <translation> Podcasts vom OPML importiert</translation>
    </message>
</context>
</TS>
