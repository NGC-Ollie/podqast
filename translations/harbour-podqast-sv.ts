<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/pages/About.qml" line="9"/>
        <source>../data/about.txt</source>
        <translation>../data/about-sv.txt</translation>
    </message>
</context>
<context>
    <name>AppMenu</name>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="9"/>
        <source>Discover</source>
        <translation>Utforska</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="17"/>
        <source>Library</source>
        <translation>Bibliotek</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="33"/>
        <source>Playlist</source>
        <translation>Spelningslista</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="25"/>
        <source>Inbox</source>
        <translation>Inkorg</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="48"/>
        <source>refreshing: </source>
        <translation>uppdaterar: </translation>
    </message>
</context>
<context>
    <name>Archive</name>
    <message>
        <location filename="../qml/pages/Archive.qml" line="140"/>
        <source>Rendering</source>
        <translation>Rendering</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="141"/>
        <source>Collecting Podcasts</source>
        <translation>Hämtar poddar</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="89"/>
        <source>History</source>
        <translation>Historik</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="60"/>
        <source>Library</source>
        <translation>Bibliotek</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="94"/>
        <source>Favorites</source>
        <translation>Favoriter</translation>
    </message>
</context>
<context>
    <name>Chapters</name>
    <message>
        <location filename="../qml/pages/Chapters.qml" line="35"/>
        <source>Chapters</source>
        <translation>Avsnitt</translation>
    </message>
    <message>
        <location filename="../qml/pages/Chapters.qml" line="94"/>
        <source>No chapters</source>
        <translation>Inga avsnitt</translation>
    </message>
    <message>
        <location filename="../qml/pages/Chapters.qml" line="95"/>
        <source>Rendering chapters</source>
        <translation>Renderar avsnitt</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="37"/>
        <source> chapters</source>
        <translation> avsnitt</translation>
    </message>
</context>
<context>
    <name>Discover</name>
    <message>
        <location filename="../qml/pages/Discover.qml" line="28"/>
        <source>Discover</source>
        <translation>Utforska</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="43"/>
        <source>Search...</source>
        <translation>Sök...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="47"/>
        <source>Tags...</source>
        <translation>Taggar...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="51"/>
        <source>Url...</source>
        <translation>URL...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="55"/>
        <source>Import...</source>
        <translation>Import...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="59"/>
        <source>Export...</source>
        <translation>Export...</translation>
    </message>
</context>
<context>
    <name>DiscoverExport</name>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="25"/>
        <source>Backup done</source>
        <translation>Säkerhetskopiering klar</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="26"/>
        <location filename="../qml/pages/DiscoverExport.qml" line="27"/>
        <source>Backup done to </source>
        <translation>Säkerhetskopiering klar till </translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="33"/>
        <source>OPML file saved</source>
        <translation>OPML-fil sparad</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="34"/>
        <location filename="../qml/pages/DiscoverExport.qml" line="35"/>
        <source>OPML file saved to </source>
        <translation>OPML-fil sparad i </translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="53"/>
        <source>Export</source>
        <translation>Exportera</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="64"/>
        <source>Backup</source>
        <translation>Säkerhetskopiera</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="67"/>
        <source>Start Backup</source>
        <translation>Starta säkerhetskopiering</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="85"/>
        <source>Save to OPML</source>
        <translation>Spara till OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="88"/>
        <source>Start OPML export</source>
        <translation>Starta OPML-export</translation>
    </message>
</context>
<context>
    <name>DiscoverImport</name>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="25"/>
        <source> Podcasts imported</source>
        <translation> poddar importerade</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="26"/>
        <location filename="../qml/pages/DiscoverImport.qml" line="27"/>
        <source> Podcasts imported from OPML</source>
        <translation> poddar importerade från OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="46"/>
        <source>Discover by Importing</source>
        <translation>Utforska via import</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="59"/>
        <source>Pick an OPML file</source>
        <translation>Välj en OPML-fil</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="60"/>
        <source>OPML file</source>
        <translation>OPML- fil</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="80"/>
        <source>Import OPML</source>
        <translation>Importera OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="82"/>
        <source>Import OPML File</source>
        <translation>Importera OPML-fil</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="100"/>
        <source>Import from Gpodder</source>
        <translation>Importera från gPodder</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="102"/>
        <source>Import Gpodder Database</source>
        <translation>Importera gPodder-databas</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="125"/>
        <source>Please note: Importing does take some time!</source>
        <translation>Notera att import tar lite tid!</translation>
    </message>
</context>
<context>
    <name>DiscoverSearch</name>
    <message>
        <location filename="../qml/pages/DiscoverSearch.qml" line="33"/>
        <source>Discover by Search</source>
        <translation>Utforska via sök</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverSearch.qml" line="62"/>
        <source>Search</source>
        <translation>Sök</translation>
    </message>
</context>
<context>
    <name>DiscoverTags</name>
    <message>
        <location filename="../qml/pages/DiscoverTags.qml" line="43"/>
        <source>Discover Tags</source>
        <translation>Utforska taggar</translation>
    </message>
</context>
<context>
    <name>DiscoverUrl</name>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="28"/>
        <source>Discover by URL</source>
        <translation>Utforska via URL</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="41"/>
        <source>Enter podcasts&apos; url</source>
        <translation>Ange podd-URL</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="49"/>
        <source>Discover</source>
        <translation>Utforska</translation>
    </message>
</context>
<context>
    <name>Favorites</name>
    <message>
        <location filename="../qml/pages/Favorites.qml" line="46"/>
        <source>Favorites</source>
        <translation>Favoriter</translation>
    </message>
</context>
<context>
    <name>FeedParserPython</name>
    <message>
        <location filename="../qml/components/FeedParserPython.qml" line="169"/>
        <source>Error</source>
        <translation>Fel</translation>
    </message>
</context>
<context>
    <name>History</name>
    <message>
        <location filename="../qml/pages/History.qml" line="45"/>
        <source>History</source>
        <translation>Historik</translation>
    </message>
    <message>
        <location filename="../qml/pages/History.qml" line="80"/>
        <source>Rendering</source>
        <translation>Rendering</translation>
    </message>
    <message>
        <location filename="../qml/pages/History.qml" line="81"/>
        <source>Collecting Posts</source>
        <translation>Hämtar poster</translation>
    </message>
</context>
<context>
    <name>Inbox</name>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="48"/>
        <source>Inbox</source>
        <translation>Inkorg</translation>
    </message>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="57"/>
        <source>Moving all posts to archive</source>
        <translation>Flyttar alla poster till arkiv</translation>
    </message>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="96"/>
        <source>No new posts</source>
        <translation>Inga nya poster</translation>
    </message>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="97"/>
        <source>Pull down to Discover new podcasts, get posts from Library, or play the Playlist</source>
        <translation>Dra neråt för att upptäcka nya poddar, hämta poster från biblioteket eller spela upp spelningslistan.</translation>
    </message>
</context>
<context>
    <name>Player</name>
    <message>
        <location filename="../qml/pages/Player.qml" line="27"/>
        <source>Audio playrate</source>
        <translation>Spelningsfrekvens</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="126"/>
        <source> chapters</source>
        <translation> avsnitt</translation>
    </message>
</context>
<context>
    <name>Podcast</name>
    <message>
        <location filename="../qml/pages/Podcast.qml" line="173"/>
        <source>Subscribe</source>
        <translation>Prenumerera</translation>
    </message>
    <message>
        <location filename="../qml/pages/Podcast.qml" line="173"/>
        <source>Configure</source>
        <translation>Konfigurera</translation>
    </message>
    <message>
        <location filename="../qml/pages/Podcast.qml" line="210"/>
        <source>Feed alternatives</source>
        <translation>Flödesalternativ</translation>
    </message>
    <message>
        <location filename="../qml/pages/Podcast.qml" line="230"/>
        <source>Latest Post</source>
        <translation>Senaste posten</translation>
    </message>
    <message>
        <location filename="../qml/pages/Podcast.qml" line="254"/>
        <source>First Post</source>
        <translation>Första posten</translation>
    </message>
</context>
<context>
    <name>PodcastItem</name>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="16"/>
        <source>Refresh</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="24"/>
        <source>Settings</source>
        <translation>Inställningar</translation>
    </message>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="32"/>
        <source>Delete</source>
        <translation>Ta bort</translation>
    </message>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="38"/>
        <source>Deleting Podcast</source>
        <translation>Tar bort podd</translation>
    </message>
</context>
<context>
    <name>PodcastSettings</name>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="25"/>
        <source>Posts</source>
        <translation>Poster</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="34"/>
        <source>Move new post to</source>
        <translation>Flytta ny post till</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="36"/>
        <source>Inbox</source>
        <translation>Inkorg</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="37"/>
        <source>Top of Playlist</source>
        <translation>Först i spelningslistan</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="38"/>
        <source>Bottom of Playlist</source>
        <translation>Sist i spelningslistan</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="39"/>
        <source>Archive</source>
        <translation>Arkiv</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="46"/>
        <source>Automatic post limit</source>
        <translation>Automatisk postbegränsning</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="63"/>
        <source>Audio playrate</source>
        <translation>Spelningsfrekvens</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="72"/>
        <source>Podcast</source>
        <translation>Podd</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="80"/>
        <source>Subscribe</source>
        <translation>Prenumerera</translation>
    </message>
</context>
<context>
    <name>Podcastsearch</name>
    <message>
        <location filename="../qml/pages/Podcastsearch.qml" line="31"/>
        <source>Search by Tag...</source>
        <translation>Sök via tagg...</translation>
    </message>
</context>
<context>
    <name>PodpostList</name>
    <message>
        <location filename="../qml/pages/PodpostList.qml" line="88"/>
        <source>Rendering</source>
        <translation>Rendering</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodpostList.qml" line="89"/>
        <source>Creating items</source>
        <translation>Skapar objekt</translation>
    </message>
</context>
<context>
    <name>PostDescription</name>
    <message>
        <location filename="../qml/pages/PostDescription.qml" line="25"/>
        <source>Open in browser</source>
        <translation>Öppna i webbläsaren</translation>
    </message>
</context>
<context>
    <name>PrefAboutMenu</name>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="6"/>
        <source>Settings</source>
        <translation>Inställningar</translation>
    </message>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="10"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
</context>
<context>
    <name>Queue</name>
    <message>
        <location filename="../qml/pages/Queue.qml" line="68"/>
        <source>Playlist</source>
        <translation>Spelningslista</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="83"/>
        <source>No posts</source>
        <translation>Inga poster</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="84"/>
        <source>Pull down to Discover new podcasts or get posts from Inbox or Library</source>
        <translation>Dra neråt för att upptäcka nya poddar eller hämta poster från inkorgen eller biblioteket</translation>
    </message>
</context>
<context>
    <name>QueuePostListItem</name>
    <message>
        <location filename="../qml/components/QueuePostListItem.qml" line="49"/>
        <source>Favorite</source>
        <translation>Favorit</translation>
    </message>
    <message>
        <location filename="../qml/components/QueuePostListItem.qml" line="65"/>
        <source>Move up</source>
        <translation>Flytta upp</translation>
    </message>
    <message>
        <location filename="../qml/components/QueuePostListItem.qml" line="73"/>
        <source>Move down</source>
        <translation>Flytta ner</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/pages/Settings.qml" line="32"/>
        <source>Global Podcast Settings</source>
        <translation>Övergripande poddinställningar</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="45"/>
        <source>Move new post to</source>
        <translation>Flytta ny post till</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="47"/>
        <source>Inbox</source>
        <translation>Inkorg</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="50"/>
        <source>Archive</source>
        <translation>Arkiv</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="61"/>
        <source>Automatic post limit</source>
        <translation>Automatisk postbegränsning</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="76"/>
        <source>Audio playrate</source>
        <translation>Spelningsfrekvens</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="90"/>
        <source>Download/Streaming</source>
        <translation>Nerladdning/Strömmning</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="165"/>
        <source>Development</source>
        <translation>Utveckling</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="176"/>
        <source>Experimental features</source>
        <translation>Experimentella funktioner</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="48"/>
        <source>Top of Playlist</source>
        <translation>Först i spelningslistan</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="49"/>
        <source>Bottom of Playlist</source>
        <translation>Sist i spelningslistan</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="107"/>
        <source>Download Playlist Posts</source>
        <translation>Ladda ner spelningslistans poster</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="120"/>
        <source>Download on Mobile</source>
        <translation>Ladda ner på mobilanslutning</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="146"/>
        <source>System</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="157"/>
        <source>Audio viewable in system</source>
        <translation>Ljud som visas i systemet</translation>
    </message>
</context>
<context>
    <name>Wizzard1</name>
    <message>
        <location filename="../qml/pages/Wizzard1.qml" line="23"/>
        <source>../data/about.txt</source>
        <translation>../data/about-sv.txt</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard1.qml" line="41"/>
        <source>Next</source>
        <translation>Nästa</translation>
    </message>
</context>
<context>
    <name>Wizzard2</name>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="29"/>
        <source>Let&apos;s start...</source>
        <translation>Låt oss börja...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="30"/>
        <source>Back to info</source>
        <translation>Tillbaka till info</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="47"/>
        <source> Podcasts imported</source>
        <translation> poddar importerade</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="48"/>
        <location filename="../qml/pages/Wizzard2.qml" line="49"/>
        <source> Podcasts imported from OPML</source>
        <translation> poddar importerade från OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="66"/>
        <source>You are now able to import old stuff. You can import from Discover lateron. Please note: Importing does take some time!</source>
        <translation>Du kan nu importera gammalt material. Du kan importera från &quot;Discover lateron&quot;. Notera att import tar lite tid!</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="95"/>
        <source>Pick an OPML file</source>
        <translation>Välj en OPML-fil</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="96"/>
        <source>OPML file</source>
        <translation>OPML-fil</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="118"/>
        <source>Import OPML</source>
        <translation>Importera OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="120"/>
        <source>Import OPML File</source>
        <translation>Importera OPML-fil</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="139"/>
        <source>Import from Gpodder</source>
        <translation>Importera från gPodder</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="142"/>
        <source>Import Gpodder Database</source>
        <translation>Importera gPodder-databas</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="168"/>
        <source>When you now start you can discover new podcasts. After that you can for example select new posts to play from the Library.</source>
        <translation>När du nu startar, kan du utforska nya poddar. Efter det, kan du t.ex. välja att spela upp nya poster från biblioteket.</translation>
    </message>
</context>
<context>
    <name>harbour-podqast</name>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="268"/>
        <source>New posts available</source>
        <translation>Nya poster tillgängliga</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="269"/>
        <source>Click to view updates</source>
        <translation>Tryck för att visa uppdateringar</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="270"/>
        <source>New Posts are available. Click to view.</source>
        <translation>Det finns nya poster. Tryck för att visa.</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="287"/>
        <location filename="../qml/harbour-podqast.qml" line="288"/>
        <source>PodQast message</source>
        <translation>PodQast-meddelande</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="289"/>
        <source>New PodQast message</source>
        <translation>Nytt PodQast-meddelande</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="315"/>
        <source> Podcasts imported</source>
        <translation> poddar importerade</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="316"/>
        <location filename="../qml/harbour-podqast.qml" line="317"/>
        <source> Podcasts imported from OPML</source>
        <translation> poddar importerade från OPML</translation>
    </message>
</context>
</TS>
