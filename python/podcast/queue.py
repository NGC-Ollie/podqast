"""
A podcast queue. This does not need to be a singleton. Queue manages the playing queue
It instanciates Podposts via Factory if needed.
"""

import sys
import os
import pyotherside
import time
import threading

sys.path.append("../")

from podcast.singleton import Singleton
from podcast.factory import Factory
from podcast.store import Store
from podcast import util

queuename = 'the_queue'

class Queue:
    """
    The podcast Queue. It has a list of podposts
    """

    def __init__(self):
        """
        Initialization
        """
        
        self.podposts = []              # Id list of podposts

    def _get_ele_pos(self, index):
        """
        get the position of an element
        """

        pos = 0
        for i in self.podposts:
            if i == index:
                return pos
            pos += 1
        return -1
    
    def save(self):
        """
        pickle this element
        """

        store = Factory().get_store()
        store.store(queuename, self)

    def _get_top(self):
        """
        get the top podpost
        """

        if self.podposts == []:
            self.top = None
        else:
            podpost = self.podposts[0]
            self.top = Factory().get_podpost(podpost)

        return self.top
    
    def insert_top(self, podpost):
        """
        Insert the podpost at top of queue. The post then is triggered to download
        it's audio file.

        podpost: the podpost to be inserted. The post then is trigge
        return: 0 - do nothing, 1 - do stop
        """
        
        if self.podposts == []:
            self.podposts.append(podpost)
        else:
            if self.podposts[0] == podpost:
                return 0
            pos = self._get_ele_pos(podpost)
            if pos >= 0:
                self.podposts.remove(podpost)
            self.podposts.insert(0, podpost)

        post = Factory().get_podpost(podpost)
        position = post.get_position / 1000
        duration = post.duration
        if duration == 0 or duration - position < 60:
            pyotherside.send("duration near position")
            post.position = 0
            
        if post.insert_date == None:
            post.insert_date = time.time()

        post.save()
        self.save()
        pyotherside.send("needDownload", podpost)
        pyotherside.send("posplay", post.get_position)
        return 1
    
    def insert_next(self, podpost):
        """
        put podpost to the second position
        """

        if len(self.podposts) <= 1:
            self.podposts.append(podpost)
        else:
            pos = self._get_ele_pos(podpost)
            if pos >= 0:
                self.podposts.remove(podpost)
            self.podposts.insert(1, podpost)

        post = Factory().get_podpost(podpost)
        position = post.get_position / 1000
        duration = post.duration
        if duration == 0 or duration - position < 60:
            pyotherside.send("duration near position")
            post.position = 0
            
        if post.insert_date == None:
            post.insert_date = time.time()

        post.save()
        self.save()
        pyotherside.send("needDownload", podpost)
        
    def insert_bottom(self, podpost):
        """
        append a podpost at the end of the queue

        podpost: podpost to be appended
        """

        pos = self._get_ele_pos(podpost)
        if pos >= 0:
            self.podposts.remove(podpost)
        self.podposts.append(podpost)

        post = Factory().get_podpost(podpost)
        position = post.get_position / 1000
        duration = post.duration
        if duration == 0 or duration - position < 60:
            pyotherside.send("duration near position")
            post.position = 0
            
        if post.insert_date == None:
            post.insert_date = time.time()

        
        post.save()
        self.save()
        pyotherside.send("needDownload", podpost)
        
    def remove(self, podpost):
        """
        remove podpost from list
        """

        if podpost in self.podposts:
            post = Factory().get_podpost(podpost)
            post.delete_file()
            post.state = 0
            post.save()
            if podpost == self.podposts[0] and len(self.podposts) > 1:
                post1 = Factory().get_podpost(self.podposts[1])
                pyotherside.send("setpos", post1.position)
            self.podposts.remove(podpost)
        else:
            Factory().get_podpost(podpost).save()

        self.save()

    def download(self, podpost):
        """
        let a podpost download
        """

        if podpost in self.podposts:
            post = Factory().get_podpost(podpost)
            for perc in post.download_audio():
                yield perc

            if podpost == self.podposts[0]:
                pyotherside.send("firstDownloaded")

    def download_all(self):
        """
        Download all posts in queue
        """

        for podpost in self.podposts:
            post = Factory().get_podpost(podpost)
            if post.file_path:
                if os.path.isfile(post.file_path):
                    continue
            pyotherside.send("needDownload", podpost)
                        
    def get_hr_time(self, position):
        """
        get the human readable time of podpost
        """

        return Factory().get_podpost(self.podposts[0]).get_hr_time(position=position)
        
    def move(self, podpost, pos):
        """
        Move a podpost to another position (drag/drop)
        podpost: the podpost
        pos: position
        """

        pos = self._get_ele_pos(podpost)
        if elepos < 0:               # element does not exist in arrax
            return
        if pos == elepos:            # element is at position
            return

        self.podposts.remove(podpost)
        self.podposts.insert(pos, podpost)
        self.save()

    def move_up(self, podpost):
        """
        move podpost one position up
        """

        ind = self._get_ele_pos(podpost)
        if ind > 1:
            self.podposts.remove(podpost)
            self.podposts.insert(ind - 1, podpost)
            self.save()
            
    def move_down(self, podpost):
        """
        move podpost one position down
        """

        ind = self._get_ele_pos(podpost)
        if ind < len(self.podposts):
            self.podposts.remove(podpost)
            self.podposts.insert(ind + 1, podpost)
            self.save()

    def get_podposts(self):
        """
        Get a list of all podposts
        """

        for podpost in self.podposts:
            yield(podpost)


    def play(self):
        """
        start playing
        """

        return self._get_top().play()

    def stop(self, position):
        """
        Stop and save position
        """

        podpost = self._get_top()
        podpost.stop(position)
        
    def position(self, position):
        """
        set top element's position in seconds
        """

        podpost = self._get_top()
        podpost.set_position(position)
        podpost.save()
        
    def pause(self, position):
        """
        pause situation
        """
        podpost = self._get_top()
        podpost.pause(position)

    def set_duration(self, duration):
        """
        Set duration of element if not set
        """

        self._get_top().set_duration(duration)
        
class QueueFactory(metaclass=Singleton):
    """
    Factory which creates a Queue if it does not exist or gets the pickle
    otherwise it returns the Singleton
    """

    def __init__(self, progname='harbour-podqast'):
        """
        Initialization
        Get the actual paths. Because we do not have a queue by now set have_queue
        to false
        """

        self.queue = None

    def get_queue(self):
        """
        Get the queue
        """

        if not self.queue:
            self.queue = Factory().get_store().get(queuename)
            if not self.queue:
                self.queue = Queue()

        return self.queue
