"""
Podpost: a single podcast post
"""

import sys
sys.path.append("../")

import hashlib
import os
from urllib import request
from urllib.parse import urlparse
import html2text
from email.utils import mktime_tz, parsedate_tz

from podcast.factory import Factory
from podcast.util import s_to_hr, tx_to_s, dl_from_url, dl_from_url_progress
from podcast.util import delete_file, format_full_date, s_to_year

import pyotherside
import time

PLAY=1
PAUSE=2
STOP=0

class Podpost:
    """
    One podcast post. It has all information needed for playing and information.
    """

    def __init__(self, entry, podurl, logo_url):
        """
        Thank you podcast providers for not using any standards. This init is
        the worst spaghetti I ever wrote...

        entry: the entry took from feedparser
        podcast: podcast url
        """

        self.podurl = podurl
        self.logo_url = logo_url
        self.logo_path = None
        if len(logo_url) > 0:
            if logo_url[0] == "/":
                self.logo_path = logo_url
        self.insert_date = None
        try:
            self.published = time.mktime(entry.published_parsed)
        except:
            self.published = None
        try:
            if not self.published:
                pyotherside.send("no published_parsed")
                self.published = mktime_tz(parsedate_tz(entry.published))
        except:
            self.published = 0

        self.title = entry.title
        if 'link' in entry:
            self.link = entry.link
        else:
            self.link = None

        self.plainpart = None
        self.htmlpart = None

        if 'content' in entry.keys():
            for cont in entry.content:
                if cont.type == 'text/html':
                    self.htmlpart = cont.value
                if cont.type == 'text/plain':
                    self.plainpart = cont.value

        if not self.plainpart:
            self.plainpart = entry.summary
        if not self.htmlpart:
            self.htmlpart = entry.summary_detail.value

        h = html2text.HTML2Text()
        h.ignore_links = True
        h.ignore_images = True

        self.plainpart = h.handle(self.plainpart)

        if 'author' in entry.keys():
            self.author = entry.author
        else:
            self.author = ""

        if 'id' in entry:
            if entry.id != '':
                self.id = hashlib.sha256(entry.id.encode()).hexdigest()
            else:
                pyotherside.send("podpost: empty id")
                self.id = hashlib.sha256(entry.summary.encode()).hexdigest()
        else:
            self.id = hashlib.sha256(entry.summary.encode()).hexdigest()

        for e in entry.enclosures:
            if e.type[:5] == "audio":
                if 'length' in e.keys():
                    self.length = e.length
                else:
                    self.length = 0
                self.type = e.type
                self.href = e.href
                # self.guid = e.guid

        self.file_path = None

        if 'itunes_duration' in entry:
            try:
                self.duration = tx_to_s(entry.itunes_duration)
            except:
                self.duration = None
        else:
            self.duration = None

        if 'psc_chapters' in entry:
            self.chapters = entry.psc_chapters.chapters
        else:
            self.chapters = []

        self.position = 0
        self.state = STOP
        self.percentage = 0
        self.favorite = False

    def get_hr_time(self, position=None):
        """
        get actual time string human readable
        """

        from podcast.queue import QueueFactory

        queue = QueueFactory().get_queue()
        if position:
            self.position = position
        self.save()
        if self.duration:
            if self.position > 0:
                add = ""
                if self.state == PLAY and self.id == queue.podposts[0]:
                    add = " playing"
                else:
                    add = " remaining"
                t = self.duration - self.position / 1000
                sthr = s_to_hr(t)
                if sthr == "":
                    return "<b>listened</b>"
                return "<b>" + sthr + add + "</b>"
            else:
                return "<b>" + s_to_hr(self.duration) + "</b>"
        return ""

    def get_data(self):
        """
        return a dict of main data
        """

        from podcast.queue import QueueFactory

        queue = QueueFactory().get_queue()
        inqueue = self.id in queue.podposts

        timestring = self.get_hr_time()

        if self.logo_path != None:
            image = self.logo_path
        else:
            image = self.logo_url

        section = format_full_date(self.published)
        date = self.published
        fdate = s_to_year(date)
        if self.insert_date:
            asection = format_full_date(self.insert_date)
            adate = self.insert_date
        else:
            asection = ""
            adate = 0
        pyotherside.send("adate, asection", adate, asection)
        pyotherside.send("date, section", date, section)
        if self.duration:
            duration = s_to_hr(self.duration)
        else:
            duration = ""

        if self.link:
            link = self.link
        else:
            link = ""

        try:
            length = self.length
        except:
            length = 0

        try:
            favorite = self.favorite
        except:
            favorite = False

        return { 'id': self.id,
                 'title': self.title,
                 'url': self.podurl,
                 'logo_url': image,
                 'link': link,
                 'description': self.plainpart,
                 'detail': self.htmlpart,
                 'timestring': timestring,
                 'date': date,
                 'fdate': fdate,
                 'section': section,
                 'adate': adate,
                 'asection': asection,
                 'length': length,
                 'duration': duration,
                 'dlperc': self.percentage,
                 'favorite': favorite,
                 'inqueue': inqueue
                }

    def save(self):
        """
        Save this element. If self.insert_date = None: set date
        """

        self.download_icon()
        store = Factory().get_store()
        store.store(self.id, self)

    def download_audio(self):
        """
        Download the audio stuff
        """

        if self.file_path and self.percentage == 100:
            if os.path.exists(self.file_path):
                return

        afilepath = Factory().afilepath
        hrefparse = urlparse(self.href)
        filename = os.path.basename(urlparse(self.href).path)
        rdot = filename.rfind(".")
        ext = filename[rdot+1:]

        name = hashlib.sha256(self.href.encode()).hexdigest() + "." + ext
        namepart = name + ".part"

        file_path = os.path.join(afilepath, name)
        file_path_part = os.path.join(afilepath, namepart)
        if os.path.isfile(file_path_part):
            if time.time() - os.path.getmtime(file_path_part) < 2 * 60:
                pyotherside.send("audio download: part file younger than 2 min")
                return
        try:
            for perc in dl_from_url_progress(self.href, file_path_part):
                self.percentage = perc
                yield perc
        except:
            self.delete_file()
            file_path = None
            self.percentage = 0

        try:
            os.rename(file_path_part, file_path)
        except:
            file_path = None
            self.percentage = 0

        self.file_path = file_path
        self.save()

    def download_icon(self):
        """
        Download icon
        """

        if self.logo_path:
            if os.path.exists(self.logo_path):
                return

        iconpath = Factory().iconpath
        rdot = self.logo_url.rfind(".")
        ext = self.logo_url[rdot+1:]
        if ext == "jpg" or ext == "png" or ext == "gif":
            name = hashlib.sha256(self.logo_url.encode()).hexdigest() + "." + ext

            logo_path = os.path.join(iconpath, name)
            try:
                dl_from_url(self.logo_url, logo_path)
                # for i in dl_from_url_progress(self.logo_url, logo_path):
                #     yield i
            except:
                logo_path = ""

            self.logo_path = logo_path
        else:
            self.logo_path = ""

    def play(self):
        """
        Start playing. Returns the audio file and it's actual position
        """

        if self.file_path and self.percentage==100:
            url = self.file_path
        else:
            url = self.href

        last_state = self.state
        self.state = PLAY
        return {
            'url': url,
            'position': self.position
            }

    def set_position(self, position):
        """
        set position
        """

        self.position = position
        self.save()

    @property
    def get_position(self):
        """
        Return position in seconds
        """

        return self.position


    def set_duration(self, duration):
        """
        set duration if not yet set
        """

        if not self.duration:
            self.duration = duration
            self.save()

    def pause(self, position):
        """
        Pause audio. Save the actual state
        """

        if position >= 0:
            self.position = position
        self.state = PAUSE
        self.save()

    def stop(self, position):
        """
        Stop the play and save the position
        """

        if position >= 0:
            self.position = position
        self.state = STOP
        self.save()

    @property
    def get_play_state(self):
        """
        get state of play. Will show the active element
        """

        return self.state

    @property
    def get_icon(self):
        """
        get url of icon
        """

        return self.logo_url

    def delete_file(self):
        """
        Delete downloaded File
        """

        if self.file_path:
            delete_file(self.file_path)
            self.percentage = 0
            self.file_path = None
        self.save()

    def toggle_fav(self):
        """
        toggle favorite flag (and create one if it not exists)
        return: bool if favorite
        """

        try:
            self.favorite = not self.favorite
        except:
            self.favorite = True

        self.save()

        return self.favorite

    @property
    def get_fav(self):
        """
        Return Favorite state
        """

        try:
            favorite = self.favorite
        except:
            favorite = False

        return favorite
