"""
Podcast
"""

import sys

sys.path.append("/usr/share/podqast/python")

from podcast.singleton import Singleton
from podcast.cache import Cache
from podcast.store import Store
from podcast.podpost import Podpost
from podcast import util
from email.utils import mktime_tz, parsedate_tz

import time
import urllib
import hashlib
import os
import feedparser
import hashlib
import pyotherside

class Podcast:
    """
    A saved podcast
    """

    def __init__(self, url, feed=None):
        """
        The podcast itself only needs it's url
        url: Url of the podcast
        """

        self.url = url
        if (url.find('tumblr.com') >= 0):
            agent = util.user_agent2
        else:
            agent = util.user_agent

        self.feed = feedparser.parse(url, agent=agent)
        
        if self.feed.bozo != 0:
            exc = self.feed.bozo_exception
            if type(exc) != feedparser.CharacterEncodingOverride:
                pyotherside.send("Podcast init: error in parsing feed")
                self.title = None
                return
        pyotherside.send("podcast init size of entries: %d" % len(self.feed.entries))

        try:
            self.title = self.feed.feed.title
        except:
            pyotherside.send("podcast: no title")
            self.title = None
            return

        self.link = self.feed.feed.link
        try:
            self.description = self.feed.feed.description
        except:
            self.description = ""

        try:
            self.logo_url = self.feed.feed.image.href
            pyotherside.send("podcast logo_url: " + self.logo_url)
        except:
            self.logo_url = "../../images/podcast.png"
            pyotherside.send("podcast logo_url error: " + str(sys.exc_info()[0]))

        self.logo_path = None

        if 'updated' in self.feed:
            self.published = mktime_tz(parsedate_tz(self.feed.updated))
        else:
            if 'Date' in self.feed.headers:
                self.published = mktime_tz(parsedate_tz(self.feed.headers['Date']))
        self.move = -1

    def feedinfo(self):
        """
        return feed information
        """

        title = self.title
        if len(title) > 50:
            title = title[:50] + "..."
        description = descriptionfull = self.description

        if len(description) > 160:
            description = description[:160] + "..."

        self.download_icon()
        if self.logo_path:
            image = self.logo_path
        else:
            image = self.logo_url

        return {
            "url": self.url,
            "title": title,
            "description": description,
            "descriptionfull": descriptionfull,
            "logo_url": image
            }

    def get_alt_feeds(self):
        """
        return all alternative feeds
        """

        altfeeds = []

        for feed in self.feed.feed.links:
            try:
                if feed.type == 'application/rss+xml' or feed.type == 'application/atom+xml':
                    if 'title' in feed:
                        title = feed.title
                    else:
                        title = ""

                    altfeeds.append({
                        "url": feed.href,
                        "title": title
                        })
            except:
                pass

        return { "altfeeds": altfeeds }

    def get_entry(self, id):
        """
        Get an entry from podcast by id
        """
        from podcast.factory import Factory

        entry = Factory().get_podpost(id)
        return entry

    def get_entries(self):
        """
        Return a list of entries
        """

        from podcast.factory import Factory
        pyotherside.send("Podcast get_entries started.")

        entries = []

        if self.logo_path:
            image = self.logo_path
        else:
            image = self.logo_url


        for entry in self.feed.entries:
            post = Podpost(entry, self.url, image)
            pyotherside.send("pid: " + post.id)
            p2 = Factory().get_podpost(post.id)
            if p2:
                post = p2
            entries.append(post.get_data())

        pyotherside.send("Podcast get_entries ended.")
        return entries

    def get_first_entry(self):
        """
        Get the first entry from podcast
        """

        from podcast.factory import Factory

        if len(self.feed.entries) < 1:
            return None
        entry = Podpost(self.feed.entries[0], self.url, self.logo_url)
        p2 = Factory().get_podpost(entry.id)
        if p2:
            entry = p2
        Factory().podpostcache.store(entry.id, entry)
        return entry

    def get_last_entry(self):
        """
        Get the last entry from podcast
        """

        from podcast.factory import Factory

        entry = Podpost(self.feed.entries[len(self.feed.entries) - 1],
                        self.url, self.logo_url)
        p2 = Factory().get_podpost(entry.id)
        if p2:
            entry = p2
        Factory().podpostcache.store(entry.id, entry)
        return entry

    def save(self):
        """
        Save this element
        """
        self.download_icon()
        PodcastFactory().store.store(self.url, self)
        PodcastFactory().podcastcache.store(self.url, self)

    def delete(self):
        """
        Remove Item from store
        """

        PodcastFactory().store.delete(self.url)

    def download_icon(self):
        """
        Download icon
        """

        from podcast.factory import Factory

        if self.logo_path:
            if os.path.exists(self.logo_path):
                return

        iconpath = PodcastFactory().iconpath
        rdot = self.logo_url.rfind(".")
        ext = self.logo_url[rdot+1:]
        if ext == "jpg" or ext == "jpeg" or ext == "png" or ext == "gif":
            name = hashlib.sha256(self.logo_url.encode()).hexdigest() + "." + ext

            logo_path = os.path.join(iconpath, name)
            try:
                util.dl_from_url(self.logo_url, logo_path)
            except:
                logo_path = ""
                pyotherside.send("could not load icon: " + str(sys.exc_info()[0]))
            self.logo_path = logo_path
        else:
            self.logo_path = ""

    def refresh(self, moveto, limit=0):
        """
        Refresh podcast and return list of new entries
        """

        pyotherside.send("refreshPost", self.title)

        feed = feedparser.parse(self.url)
        if feed.bozo != 0:
            pyotherside.send("refreshPost", None)
            return []
        
        if 'updated' in feed:
            published = mktime_tz(parsedate_tz(feed.updated))
        else:
            if 'Date' in feed.headers:
                published = mktime_tz(parsedate_tz(feed.headers['Date']))

        first = self.get_first_entry()
        pyotherside.send("refresh published:", published, self.published)
        if published <= self.published:
            pyotherside.send("refreshPost", None)
            return []

        self.feed = feed
        self.published = published
        self.save()

        entries = []
        if self.logo_path:
            image = self.logo_path
        else:
            image = self_logo_url

        pyotherside.send("refresh first.id", first.id)

        move = self.move
        if move < 0:
            move = moveto

        pyotherside.send("Moveto is %d" % move)

        limitcount = 0
        for entry in self.feed.entries:
            thehash = hashlib.sha256(entry.id.encode()).hexdigest()
            pyotherside.send("refresh entry.id", thehash)
            pyotherside.send("first.id", first.id)
            if thehash == first.id:
                pyotherside.send("refreshPost", None)
                break

            post = Podpost(entry, self.url, image)
            post.save()
            entries.append({
                "id": post.id,
                "post": post.get_data(),
                "pctitle": self.title,
                "pstitle": post.title,
                "move": move
                })
            try:
                if self.autolimit:
                    limit = self.autolimit
            except:
                self.autolimit = None
            
            if limit != 0 and len(entries) >= limit:
                pyotherside.send("apperror", "Auto post limit reached!")
                break

        pyotherside.send("Fount %d new entries." % len(entries))
        pyotherside.send("refreshPost", None)
        return entries

    def set_params(self, params):
        """
        Set the Parameters
        """

        self.move = params["move"]
        self.autolimit = params["autolimit"]
        self.playrate = params["playrate"]
        self.save()

    def get_params(self):
        """
        Return a set of parameters
        """

        try:
            if self.subscribed:
                subscribed = True
            else:
                subscribed = False
        except:
            subscribed = False
        try:
            autolimit = self.autolimit
        except:
            autolimit = self.autolimit = None
        try:
            playrate = self.playrate
        except:
            playrate = self.playrate = 0
            
        return {
            "move": self.move,
            "subscribed": subscribed,
            "autolimit": autolimit,
            "playrate": playrate
            }

class PodcastFactory(metaclass=Singleton):
    """
    Helping Factory
    """

    def __init__(self, progname='harbour-podqast'):
        """
        Initialization
        """

        self.podcastcache = Cache(limit=50)
        home = os.path.expanduser('~')
        xdg_data_home = os.environ.get('XDG_DATA_HOME', os.path.join(home, '.local', 'share'))
        xdg_config_home = os.environ.get('XDG_CONFIG_HOME', os.path.join(home, '.config'))
        xdg_cache_home = os.path.join('XDG_CACHE_HOME', os.path.join(home, '.cache'))

        self.data_home = os.path.join(xdg_data_home, progname)
        self.config_home = os.path.join(xdg_config_home, progname)
        self.cache_home = os.path.join(xdg_cache_home, progname)

        if 'PODQAST_HOME' in os.environ:
            home = os.environ['PODQAST_HOME']
            self.data_home = self.config_home = self.cache_home = home

        self.iconpath = os.path.join(self.data_home, 'icons')
        storepath = os.path.join(self.data_home, 'store')
        util.make_directory(self.data_home)
        util.make_directory(storepath)
        util.make_directory(self.iconpath)

        self.store = Store(storepath)

    def get_podcast(self, url):
        """
        Get a new podcast instance with name url
        """

        podcast = self.podcastcache.get(url)
        if not podcast:
            pyotherside.send("PodcastFactory: not in cache")
            podcast = self.store.get(url)
            if podcast:
                pyotherside.send("PodcastFactory: From disk")
            else:
                podcast = Podcast(url)
                if podcast.title:
                    pyotherside.send("PodcastFactory: New instance")
                else:
                    return None
            pyotherside.send("PodcastFactory: store to cache")
            self.podcastcache.store(url, podcast)
        else:
            pyotherside.send("PodcastFactory: from cache")
        return podcast
